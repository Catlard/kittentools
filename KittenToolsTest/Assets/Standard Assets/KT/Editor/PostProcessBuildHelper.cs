﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor.Callbacks;
using System;
using System.Collections;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine.Networking;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;

/* this performs the exceedingly useful function of automating bundleVersionCode and 
CFBundleVersion numbers on mobile builds, so you don't have to. This could screw up
your numbering system if you already have one, but if you don't, you never need worry
about collisions! */

public class PostProcessBuildHelper : IPreprocessBuildWithReport
{

    public int callbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildReport report)
    {
      if(report.summary.platform == BuildTarget.Android) {
        PlayerSettings.Android.bundleVersionCode = (int) TimeUtils.GetUnixTime();
        Debug.Log("OnPreprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);
      }
    }
}

public class TOS_BuildPostProcessor
{
  [PostProcessBuild]
  public static void OnPostprocessBuild (UnityEditor.BuildTarget buildTarget, string buildPath)
  {
   
    if(buildTarget == UnityEditor.BuildTarget.iOS) {
        Debug.Log ("Running Postprocessor");
        #if UNITY_IOS
          string projPath = Path.Combine(buildPath, "Info.plist");
          PlistDocument plist = new PlistDocument();
          plist.ReadFromString(File.ReadAllText(projPath));
          PlistElementDict rootDict = plist.root;
          rootDict.SetString("CFBundleVersion", TimeUtils.GetUnixTime().ToString());
          rootDict.SetBoolean ("ITSAppUsesNonExemptEncryption", false);
          File.WriteAllText(projPath, plist.WriteToString());
        #endif
    }
    if(buildTarget == UnityEditor.BuildTarget.Android) {
        #if UNITY_ANDROID
          
        #endif
    }

  }

  
}

#endif