﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.Linq;


[System.Serializable]
public class AStarCell {

    
    public float _density = 0;
    public AStarCell _previousCell;

    public int _col;
    public int _row;  

    public Vector2 _position; //imaginary numbers. assumes 0, 0 is at vector2.zero

    public float _homePathValue = 0;
    public float _destinationPathValue = 0;
    public bool _isOnSolutionPath;

    public float GetTravelCost() {
        return _homePathValue + _destinationPathValue + _density;
    }

    public void Reset(bool resetDensity) {
        _homePathValue = 0;
        _destinationPathValue = 0;
        _isOnSolutionPath = false;
        _previousCell = null;
        if(resetDensity) {
            _density = 0;
        }
    }

    public AStarCell(int col, int row) {
        _col = col;
        _row = row;
        _position = new Vector2(col * 10, row * 10);
    }

    override public string ToString() {
        return "(" + _col + "," + _row + ")";
    }



    public string GetDebugViewString() {
        return "<color=#0000FF>" + MathUtils.RoundTo(GetTravelCost(), 1f).ToString() + "</color>\n" +
            "<color=#00FF00>" + MathUtils.RoundTo(_homePathValue, 1f).ToString() + "</color>\n" +
            "<color=#FF0000>" + MathUtils.RoundTo(_destinationPathValue, 1f).ToString() + "</color>";
    }

}

[System.Serializable]
public class AStarSolver {

    public enum AStarSolverState {UNSOLVED, SOLVED, NO_SOLUTION}
    public AStarSolverState _state = AStarSolverState.UNSOLVED;


    public AStarCell[,] _cells; 
    public int _width;
    public int _height;
    public float _wallDensity = 100; //how high density needs to be so that it's a wall.
    public int _stepsCalculated = 0;
    public float _efficiency = 1; //see reseet function for explanation and vid
    public bool _canCutCornersAroundWalls = false;

    public AStarCell _startCell;
    public AStarCell _finishCell;

    public List<AStarCell> _openCells;
    public List<AStarCell> _closedCells;
    public List<AStarCell> _wallCells; //can be retained on reset.

    Vector2Int right = new Vector2Int(1, 0);
    Vector2Int left = new Vector2Int(-1, 0);
    Vector2Int top = new Vector2Int(0, 1);
    Vector2Int bottom = new Vector2Int(0, -1);

    public List<Vector2Int> GetSolutionPath() {
        if(_state != AStarSolverState.SOLVED) {
            Debug.LogWarning("Getting solution from solver that has not completed finding a path.");
        }
        var pathCoordinatesInOrder = new List<(int, int)>();
        AStarCell cellOnPath = _finishCell;
        while(cellOnPath != null) {
            pathCoordinatesInOrder.Add((cellOnPath._col, cellOnPath._row));
            cellOnPath = cellOnPath._previousCell;
        }
        pathCoordinatesInOrder.Reverse();


        return pathCoordinatesInOrder.Select(coord => new Vector2Int(coord.Item1, coord.Item2)).ToList();
    }

    //after calling this function, you can set a new start and finish function, and call step again.
    //if you want to reconfigure the walls, then set resetDensitiesAndWalls to true.

    
    //https://youtu.be/pKnV6ViDpAI?t=339
    public AStarSolver Reset(bool resetDensitiesAndWalls) {
        _openCells = new List<AStarCell>();
        _closedCells = new List<AStarCell>();

        _startCell = null;
        _finishCell = null;
        _stepsCalculated = 0;

        for(int col = 0; col < _width; col++) {
            for(int row = 0; row < _height; row++) {
                _cells[col, row].Reset(resetDensitiesAndWalls);
            }
        }

        if(resetDensitiesAndWalls || _wallCells == null) {
            _wallCells = new List<AStarCell>();
        } else {
            foreach(AStarCell wall in _wallCells) {
                _closedCells.Add(wall);
            }
        }
        return this;
    }

    //note that the efficiency parameter is a multiplier for the cost of the 
    //distance of any cell back towards home --this makes it give more direct paths, 
    //but work more slowly. it also is used to inversely multiply the destination costs.
    public AStarSolver SetEfficiency(float efficiency) {
        _efficiency = efficiency;
        return this;
    }

    //if there's a wall, do we have to go manhattan around it, or can we cut through it on diag?
    public AStarSolver SetCanCutCornersAroundWalls(bool canCut) {
        _canCutCornersAroundWalls = false;
        return this;
    }

    public AStarSolver(int width, int height) {
        _width = width;
        _height = height;
        _cells = new AStarCell[width, height];
        for(int col = 0; col < _width; col++) {
            for(int row = 0; row < _height; row++) {
                _cells[col, row] = new AStarCell(col, row);
            }
        }
        Reset(false);
    }

    public void SetWall(int c, int r) {
        _cells[c, r]._density = _wallDensity;
        _closedCells.Add(_cells[c, r]);
        _wallCells.Add(_cells[c, r]);
        // Debug.Log("Set wall:" + c + ", " + r);
    }

    public void SetExtraTravelCost(int c, int r, float extraTravelCost) {
        AStarCell denseCell = _cells[c, r];
        // denseCell.SetColor(Color.Lerp(Color.white, Color.black, extraTravelCost/_wallDensity));
        denseCell._density = extraTravelCost;
    }

    public void SetStart(Vector2Int pos) {
        _startCell = _cells[pos.x, pos.y];
        // _startCell.SetColor(Color.green);
        _openCells.Add(_startCell);
    }

    public void SetFinish(Vector2Int pos) {
        _finishCell = _cells[pos.x, pos.y];
        // _finishCell.SetColor(Color.red);
    }

    public float GetDiagonalDistance(AStarCell a, AStarCell b) {
        return GridUtils.GetDiagonalDistance(a._col, a._row, b._col, b._row);
    }

    //if further stepping is continued, returns true. if it's solved or insoluble, returns false;
    public bool Step() {

        if(_finishCell._previousCell != null) //check for solved state
            return false; //been solved!
        else if(_openCells.Count == 0) //if there's no cheapest one, we're out of cells. unsolveable.
            return false;

        //find cheapest cell to check next.
        AStarCell cheapest = null;
        foreach(AStarCell cell in _openCells) {
            if(cheapest == null) { 
                cheapest = cell;
            } else if(cell.GetTravelCost() < cheapest.GetTravelCost()) {
                cheapest = cell;
            }
        }

        // Debug.Log("Cheapest was " + cheapest.ToString());

        //get coords surrounding. don't care if open or closed at this point.
        List<(int, int)> surroundingCoords = GridUtils.GetSurroundingCoordinates(
            cheapest._col, cheapest._row, _width, _height
        );

        if(!_canCutCornersAroundWalls) { //adjacents cannot be diagonals where a wall is in a cardinal direction.
            Vector2Int middle = new Vector2Int(cheapest._col, cheapest._row);
            List<(int, int)> toRemove = new List<(int, int)>();

            for(int i = 0; i < surroundingCoords.Count; i++) {
                Vector2Int coord = new Vector2Int(surroundingCoords[i].Item1, surroundingCoords[i].Item2);
                Vector2Int diff = coord - middle;
                bool isWall = _wallCells.Contains(_cells[coord.x, coord.y]);

                if(isWall) {
                    if(diff == right) { //if on right side, remove right diags.
                        toRemove.Add((middle.x + 1, middle.y + 1));
                        toRemove.Add((middle.x + 1, middle.y - 1));
                    } else if(diff == left) {
                        toRemove.Add((middle.x - 1, middle.y + 1));
                        toRemove.Add((middle.x - 1, middle.y - 1));
                    } else if(diff == top) {
                        toRemove.Add((middle.x + 1, middle.y + 1));
                        toRemove.Add((middle.x - 1, middle.y + 1));
                    } else if(diff == bottom) {
                        toRemove.Add((middle.x + 1, middle.y - 1));
                        toRemove.Add((middle.x - 1, middle.y - 1));
                    }
                }
            };

            //now we remove invalid diags from the list.
            surroundingCoords = surroundingCoords.Where(x => {
                foreach((int, int) potentiallyInvalid in toRemove) {
                    if(potentiallyInvalid.Equals(x)) return false; //see ya, it's removed
                }
                return true; //keep it!
            }).ToList(); 

        }

        //give home and destination values to these cells, and declare them open if available.
        foreach(var t in surroundingCoords) {
            AStarCell adjacent = _cells[t.Item1, t.Item2];
            
            if(_closedCells.Contains(adjacent)) { //skip closed
                // Debug.Log(adjacent._col + "," + adjacent._row + " was closed. Can't be next.");
            } else {
                if(!_openCells.Contains(adjacent)) { //add a new one.
                    _openCells.Add(adjacent);
                    // Debug.Log("Added to open list. " + adjacent._col + "," + adjacent._row + " had previous cell " + cheapest._col + "," + cheapest._row);
                    adjacent._previousCell = cheapest;
                    adjacent._homePathValue = cheapest._homePathValue + GetDiagonalDistance(adjacent, cheapest);
                    adjacent._destinationPathValue = GetDiagonalDistance(adjacent, _finishCell);
                    adjacent._homePathValue *= _efficiency;
                    adjacent._destinationPathValue /= _efficiency;
                }
               
                //check if cheaper.
                AStarCell prev = adjacent._previousCell;
                if(prev != null && cheapest.GetTravelCost() < prev.GetTravelCost()) {
                    adjacent._previousCell = cheapest;
                }

                if(adjacent == _finishCell) {
                    _state = AStarSolverState.SOLVED;
                    AStarCell path = adjacent;
                    while(path != null) {
                        path._isOnSolutionPath = true;
                        path = path._previousCell;
                    }
                    return false;
                }
            }
        }

        //stops you from checking it again.
        _openCells.Remove(cheapest);
        _closedCells.Add(cheapest);

        //no solution...?
        if(_openCells.Count == 0) {
            _state = AStarSolverState.NO_SOLUTION;
            return false;
        }

        
        _stepsCalculated ++;
        return true;

    }

   
}
                   