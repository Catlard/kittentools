﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.Linq;

public class AStarTest : MonoBehaviour
{

    public Sprite _circle;
    public AStarSolver _aStarSolver;
    public Fancy2DArrayOfValues<Vector2> _positionGrid;

    private Transform _renderParent;
    public List<GameObject> _objectsInRender;


    // Start is called before the first frame update
    void Start()
    {
        _renderParent = GameObject.Find("RenderParent").transform;
        _objectsInRender = new List<GameObject>();

        Bounds b = Camera.main.OrthographicBounds();
        b.extents = new Vector3(b.extents.y, b.extents.y, 0);
        b.extents *= .8f;
        
        int width = 10; int height = 10;

        // _positionGrid = new Grid(width, height, b);
        _positionGrid = GridUtils.MakePositionGrid(new Vector2Int(width, height), new Vector2(b.extents.x/width*2, b.extents.y/height*2));
        _aStarSolver = new AStarSolver(width, height);


        // _aStarGrid.SetWall(0,5);
        _aStarSolver.SetWall(1,5);
        _aStarSolver.SetWall(2,5);
        _aStarSolver.SetWall(3,5);
        _aStarSolver.SetWall(4,5);
        _aStarSolver.SetWall(5,5);
        _aStarSolver.SetWall(6,5);
        _aStarSolver.SetWall(7,5);
        _aStarSolver.SetWall(8,5);
        _aStarSolver.SetWall(9,5);

        // _aStarSolver.SetStart(9,0);
        // _aStarSolver.SetFinish(9,9);

        Render();
        // SolveImmediately();
    }

    private void SolveImmediately() {
        float calcTime = Time.realtimeSinceStartup;
        while(_aStarSolver.Step()) {};
        calcTime = Time.realtimeSinceStartup - calcTime;
        print("Path calculated immediately with result: " + _aStarSolver._state + 
              " after " + _aStarSolver._stepsCalculated + " in " + calcTime + " secs." );
        Render();
    }

    private void RenderGridPoint(Vector2 pos, int col, int row) {

        AStarCell cell = _aStarSolver._cells[col, row];

        Transform gp = new GameObject("gp").transform;
        gp.localScale = Vector3.one * 2;
        gp.name = col + ", " + row;
        gp.SetParent(_renderParent);
        gp.position = pos;

        SpriteRenderer s = gp.gameObject.AddComponent<SpriteRenderer>();
        s.sprite = _circle;

        
        if(cell._isOnSolutionPath) {
            s.color = Color.blue;
        } else if(_aStarSolver._startCell == cell) {
            s.color = Color.green;
        } else if(_aStarSolver._finishCell == cell) {
            s.color = Color.red;      
        } else if(_aStarSolver._openCells.Contains(cell)) {
            s.color = Color.yellow;
        } else if(_aStarSolver._closedCells.Contains(cell)) {
            s.color = Color.gray;
        }

        s.color = Color.Lerp(s.color, Color.white, .5f);
        s.color = Color.Lerp(s.color, Color.black, cell._density / _aStarSolver._wallDensity);

        if(!_aStarSolver._openCells.Contains(cell)) return;

        GameObject text = new GameObject("text");
        text.transform.SetParent(gp.transform);
        TextMeshPro debugText = text.AddComponent<TextMeshPro>();
        debugText.text = cell.GetDebugViewString();
        debugText.fontSize = 1.5f;
        debugText.lineSpacing = -17.2f;
        debugText.color = Color.red;
        debugText.sortingOrder = 100;
        debugText.alignment = TextAlignmentOptions.Midline;
        RectTransform rt = text.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(1,1);
        text.transform.localPosition = Vector3.zero;
    }

    void Update() {
        if(Input.GetKey(KeyCode.Space)) {
            bool shouldContinue = _aStarSolver.Step();
            if(!shouldContinue) {
                print("Finished: " + _aStarSolver._state + " after " + _aStarSolver._stepsCalculated);
            }
            Render();
        }
    }

    private void Render() {
        
        for(int i = _renderParent.childCount-1; i>=0; i--) {
            GameObject.Destroy(_renderParent.GetChild(i).gameObject);
        }

        _positionGrid.EachElement((Vector2 elem, Vector2Int pos, int x, int y) => {
            RenderGridPoint(elem, x, y);
        });
    }
    
}

