﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* 
 modifies the scale of an object based on the scale. 
 doesn't work dynamically if it isn't the right device type,
 but that's life, baby! Feel free to add that functionality.

*/

[System.Serializable]
public class DeviceScaleDictionary : SerializableDictionary<DeviceType, Vector3> {}

public class ModifyScaleForDevice : MonoBehaviour {

	[Inspectionary]
	public DeviceScaleDictionary _deviceScales;

	public void Init(DeviceType t) {
		Transform tr = GetComponent<Transform>();
		Vector3 mult = Vector3.one;
		if(_deviceScales.TryGetValue (t, out mult)) {
			tr.localScale = VectorUtils.Scale(tr.localScale, mult);
		}	
	}
}
