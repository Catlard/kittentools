﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/* 
detects device, even in the editor, using aspect ratios. If the aspect ratio is 
not exactly one of the ones provided, then it finds the closest one. To add
a specific aspect ratio, add a DeviceType enum, and then a line to the deviceinfo array.

The deviceInfo should be checked once at startup by calling GetInfo(). Then use the
returned data object any way you want.
*/

public enum DeviceType {
	APPLE_IPAD_PORTRAIT,
	APPLE_IPAD_LANDSCAPE,
	APPLE_IPHONE_LANDSCAPE,
	APPLE_IPHONE_PORTRAIT,
	APPLE_IPHONE_X_LANDSCAPE,
	APPLE_IPHONE_X_PORTRAIT,
	APPLE_IPHONE_XR_LANDSCAPE,
	APPLE_IPHONE_XR_PORTRAIT,
	APPLE_IPAD_11_LANDSCAPE,
	APPLE_IPAD_11_PORTRAIT,
	ANDROID_3_2,
	ANDROID_8_5,
	ANDROID_5_3,
	ANDROID_16_10,
	UNKNOWN,
}



public static class DeviceDetector {

	private static float _tolerance = .05f;

	private static DeviceInfo _currentInfo;

	//all device info stored here.
	private static DeviceInfo[] _infos = new DeviceInfo[]{
		new DeviceInfo(DeviceType.APPLE_IPAD_LANDSCAPE, new Vector2(4,3)),
		new DeviceInfo(DeviceType.APPLE_IPAD_PORTRAIT,  new Vector2(3,4)),
		new DeviceInfo(DeviceType.APPLE_IPHONE_LANDSCAPE, new Vector2(16,9)),
		new DeviceInfo(DeviceType.APPLE_IPHONE_PORTRAIT,  new Vector2(9,16)),
		new DeviceInfo(DeviceType.APPLE_IPHONE_X_LANDSCAPE, new Vector2(19.5f,9)),
		new DeviceInfo(DeviceType.APPLE_IPHONE_X_PORTRAIT,  new Vector2(9,19.5f)),
		//xs same as x
		new DeviceInfo(DeviceType.APPLE_IPHONE_XR_LANDSCAPE,new Vector2(896, 414)),
		new DeviceInfo(DeviceType.APPLE_IPHONE_XR_PORTRAIT, new Vector2(414, 896)),
		//11 same as 12.9
		new DeviceInfo(DeviceType.APPLE_IPAD_11_LANDSCAPE, new Vector3(4.3f, 3)),
		new DeviceInfo(DeviceType.APPLE_IPAD_11_PORTRAIT, new Vector3(3, 4.3f)),

		new DeviceInfo(DeviceType.ANDROID_3_2,  new Vector2(3,2)),
		new DeviceInfo(DeviceType.ANDROID_8_5,  new Vector2(8,5)), //fattest phoney size 
		new DeviceInfo(DeviceType.ANDROID_5_3,  new Vector2(5,3)), //thinnest tablety size

	};


	public static DeviceInfo GetInfo(bool force = false) {
		if (_currentInfo == null || force) {
			DetermineDevice ();
		}
		return _currentInfo;
	}

	private static void DetermineDevice() {

		DeviceInfo returned = new DeviceInfo (DeviceType.UNKNOWN, new Vector2(Screen.width, Screen.height)); 
		float myResolution = (float) Screen.width / Screen.height;
        

		foreach(DeviceInfo di in _infos) {
			float sRes = di._aspectRatio.x / di._aspectRatio.y; 
			// DBug.print (di._type + " = " + sRes + " vs " + myResolution); 
			if (Mathf.Abs (sRes - myResolution) < _tolerance) {
				returned = di;
			}
		}

		if (returned._type == DeviceType.UNKNOWN) {
			foreach (DeviceInfo info in _infos) {
				Vector2 r = info._aspectRatio;
				if (((float)r.x / r.y) - myResolution < _tolerance) {
					returned._aspectRatio = r;
				} else if (((float)r.y / r.x) - myResolution < _tolerance) {
					returned._aspectRatio = new Vector2 (r.y, r.x);
				}
			}

			if (returned._aspectRatio == Vector2.zero) {
				if (Screen.width > Screen.height) {
					returned._aspectRatio = new Vector2 (Screen.width, Screen.height);
				} else {
					returned._aspectRatio = new Vector2 (Screen.height, Screen.width);
				}
			}

			returned.Recalculate();
		}

		Dictionary<DeviceType, bool> thinnerThan = new Dictionary<DeviceType, bool>();
		foreach(DeviceInfo di in _infos) {
			// Debug.Log(di._type + " is " + di._thinness + " vs " + returned._thinness);
			thinnerThan.Add(di._type, returned._thinness > di._thinness);
		}

		returned._thinnerThan = thinnerThan;

		_currentInfo = returned;
	}




}

[System.Serializable]
public class DeviceInfo {
	public DeviceType _type;
	public Vector2 _aspectRatio;
	public float _thinness;
	public float _howMuchThinnerThaniPad;
	public Dictionary<DeviceType, bool> _thinnerThan;

	override public string ToString() {
		return _type + ": Aspect = " + _aspectRatio + ", Thinness = " + _thinness;
	}

	public DeviceInfo (DeviceType t, Vector2 ratio) {
		_type = t;
		_aspectRatio = ratio;
		Recalculate();
		
	}

	public void Recalculate() {
		_thinness = _aspectRatio.x / _aspectRatio.y;
		_howMuchThinnerThaniPad =  1.333f/_thinness;
	}
	

	public bool ThinnerThan(DeviceType t) {
		bool has = _thinnerThan.ContainsKey(t);
		if(has) {
			return _thinnerThan[t];
		} else {
			Debug.LogError("No device of type " + t + " in comparisons. Returning true.");
			return true;
		}

	}

	// thinnest 1 2 3 4 fattest

	public bool FatterThan(DeviceType t) {
		return !ThinnerThan(t) && t != _type;
	}

	public bool ThinnerThanOrEqualTo(DeviceType t) {
		return t == _type || ThinnerThan(t);
	}

	public bool FatterThanOrEqualTo(DeviceType t) {
		return t == _type || FatterThan(t);
	}
}
