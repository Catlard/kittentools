﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using DG.Tweening;
using UnityEngine.Events;
using System;
using System.Linq;


/* You probably won't need to use this class, since all the important functionality is 
exposed in the Routines.cs file. This file manages all the lookups and links between
KTBehaviors and tweens/coroutines. If you want to pause all the routines on a ktbehavior,
this file knows how to find all the routines running there. MEC can do this with a 
reference to a monobehavior, but in order to pause both tweens AND coroutines with
one string identifier, we use this class to link them together, and give them one
access point from the tag. */


[System.Serializable]
public class RoutineManager {
 
	private struct beh_iid {
		public int iid;
		public MonoBehaviour beh;
	}

	private bool _debug;
	private Dictionary<int, List<string>> _crossReference; //for linking objects with tags.
	private Dictionary<int, MonoBehaviour> _iidLookup;
	private Dictionary<string, beh_iid> _tagLookup;
	private string _managerID;

	[HideInInspector] public Action<bool> _onPause;

	public RoutineManager(string location, bool debug = false) {
		_managerID = StringUtils.MakeTag(location);
		_debug = debug;
		Clear();
	}

	//////////////////////////////////////////////////////////////
	//DEBUGGIN' DEBUGGIN' DEBUGGIN' DEBUGGIN' DEBUGGIN' DEBUGGIN' DEBUGGIN'
	//////////////////////////////////////////////////////////////

	private void Log(object msg, bool dump = false) {
		if(!_debug) return;
		string toLog = "ROUTINES " + _managerID + ": " + msg.ToString();
		if(dump) toLog += "\n\n" + Dump();
		Debug.Log(toLog);
	}

	public string Dump() {
		Clean();
		string msg = "Routine manager had: ";
		foreach(KeyValuePair<int, List<string>> kvp in _crossReference) {
			msg += "\n" + kvp.Key.ToString() + ": ";
			for(int i = 0; i < kvp.Value.Count; i++) {
				msg += kvp.Value[i] + ", ";
			}
		}
		return msg;
	}

	//////////////////////////////////////////////////////////////
	//CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP CLEANUP
	//////////////////////////////////////////////////////////////	

	//reset the collections.
	public void Clear() {
		_crossReference = new Dictionary<int, List<string>>();
		_iidLookup = new Dictionary<int, MonoBehaviour>();
		_tagLookup = new Dictionary<string, beh_iid>();
	}

	//clean dead references to monobehaviors
	public void Clean() {
		List<string> goneTags = new List<string>();
		List<int> goneIIDs = new List<int>();
		foreach(KeyValuePair<string, beh_iid> kvp in _tagLookup) {
			if(kvp.Value.beh == null) {
				goneTags.Add(kvp.Key);
				goneIIDs.Add(kvp.Value.iid);
			}
		}
		for(int i = 0; i < goneTags.Count; i++) {
			string tag = goneTags[i];
			int iid = goneIIDs[i];
			_crossReference.Remove(iid);
			_iidLookup.Remove(iid);
			_tagLookup.Remove(tag);
		}
	}


	//////////////////////////////////////////////////////////////
	//REGISTER REGISTER REGISTER REGISTER REGISTER REGISTER REGISTER
	//////////////////////////////////////////////////////////////	

	public string Register(MonoBehaviour m, string s) {
		s = StringUtils.MakeTag(s); 
		int iid = m.GetInstanceID();
		if(!_crossReference.ContainsKey(iid))
			_crossReference[iid] = new List<string>();
		_crossReference[iid].Add(s);
		if(!_iidLookup.ContainsKey(iid))
			_iidLookup.Add(iid, m);
		beh_iid b = new beh_iid();
		b.beh = m;
		b.iid = iid;
		if(!_tagLookup.ContainsKey(s))
			_tagLookup.Add(s, b);
		return s;
	}

	public void Unregister(string s) {
		if(!_tagLookup.ContainsKey(s)) return;
		int iid = _tagLookup[s].iid;
		_tagLookup.Remove(s);
		_crossReference[iid].Remove(s);
	}

	public void UnregisterAll(MonoBehaviour m) {
		int iid = m.GetInstanceID();
		List<string> toRemove = new List<string>();
		if(_crossReference.ContainsKey(iid)) {
			foreach(string tag in _crossReference[iid]) {
				toRemove.Add(tag);
			}
		}
		foreach(string tag in toRemove)
			Unregister(tag);
	}
	
	//////////////////////////////////////////////////////////////
	//KILL KILL KILL KILL KILL KILL KILL KILL KILL KILL KILL KILL 
	//////////////////////////////////////////////////////////////


	public void Kill(string tag) {
		// Debug.Log("Killing: " + tag);
		DOTween.Kill(tag);
		Timing.KillCoroutines(tag);
	}

	//this calls the kill by string tag function. the only connection tags have to monobehaviors is what we know when they are added.
	public void Kill(MonoBehaviour m) {
		Clean();
		int iid = m.GetInstanceID();
		if(_crossReference.ContainsKey(iid)) {
			foreach(string tag in _crossReference[iid]) {
				Kill(tag);
			}
		}
	}

	public void KillAll() {
		Clean();
		foreach(KeyValuePair <int, List<string>> kvp in _crossReference) {
			foreach(string s in kvp.Value) {
				Kill(s);
			}
		}
		Log("Killed all.", true);
	}

	//////////////////////////////////////////////////////////////
	//RESUME RESUME RESUME RESUME RESUME RESUME RESUME RESUME RESUME RESUME RESUME RESUME 
	//////////////////////////////////////////////////////////////


	public void Resume(string tag) {
		DOTween.Play(tag);
		Timing.ResumeCoroutines(tag);
	}

	//this calls the kill by string tag function. the only connection tags have to monobehaviors is what we know when they are added.
	public void Resume(MonoBehaviour m) {
		Clean();
		int iid = m.GetInstanceID();
		if(_crossReference.ContainsKey(iid)) {
			foreach(string tag in _crossReference[iid]) {
				Resume(tag);
			}
		}
	}

	public void ResumeAll() {
		Clean();
		foreach(KeyValuePair <int, List<string>> kvp in _crossReference) {
			foreach(string s in kvp.Value) {
				Resume(s);
			}
		}
		Log("Resumed all.", true);

	}



	//////////////////////////////////////////////////////////////
	//PAUSE PAUSE PAUSE PAUSE PAUSE PAUSE PAUSE PAUSE PAUSE PAUSE PAUSE PAUSE 
	//////////////////////////////////////////////////////////////

	public void Pause(string tag) {
		// Log("Paused: " + tag);
		DOTween.Pause(tag);
		Timing.PauseCoroutines(tag);
	}

	//this calls the kill by string tag function. the only connection tags have to monobehaviors is what we know when they are added.
	public void Pause(MonoBehaviour m) {
		Clean();
		int iid = m.GetInstanceID();
		if(_crossReference.ContainsKey(iid)) {
			foreach(string tag in _crossReference[iid]) {
				Pause(tag);
			}
		}
	}

	public void PauseAll() {
		Clean();
		foreach(KeyValuePair <int, List<string>> kvp in _crossReference) {
			foreach(string s in kvp.Value) {
				Pause(s);
			}
		}
		Log("Paused all.", true);
	}

}
