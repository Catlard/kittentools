﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A useful little data type that only returns true once.

public class Consumabool {
   public bool _consumed = false;
   
   public bool Consume() {
       if(!_consumed) {
           _consumed = true;
           return true;
       } else return false;
   }
}
