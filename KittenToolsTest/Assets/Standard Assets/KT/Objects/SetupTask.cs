﻿/* A helper class that measures time before a task is completed. 
I use these to measure how fast startup is in my app. */

using UnityEngine.Events;
using UnityEngine;
using System.Collections.Generic;
using System;

public class SetupTask {
	public string _name;
	public UnityAction<List<object>> _action;
	public List<object> _actionParams;
	public float _startTime;
	public float _duration;
	public bool _complete;

	public void Error(string msg) {
		Debug.LogError("ERROR WITH TASK: " + _name + ": " + msg);
	}

	public float OnComplete() {
		_duration = TimeUtils.GetDuration(_startTime);
		_complete = true;
		return _duration;
	}

	public void OnBegin(Action<SetupTask> onComplete) {
		_startTime = Time.realtimeSinceStartup;
		_actionParams.Add (this); // the last object is always the startuptask sent.
		_actionParams.Add (onComplete); //the thing we do after.
		_action.Invoke(_actionParams);
	}

	public SetupTask(UnityAction<List<object>> action, List<object> actionParams, string name) {
		_action = action;
		_name = name;
		_actionParams = actionParams;
	}
}
