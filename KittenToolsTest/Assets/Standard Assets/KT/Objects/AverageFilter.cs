﻿using System.Collections;


//A useful data object that can make noisy data smoother. Useful for Accelerometer input, for example.
//This creates a kind of exponential lerp.

public class AverageFilter {

	private float[] _values;
	private int _ndx = 0;
	private int _size;


	public float _lastValue;
	

	public AverageFilter(int size, float initValue) {
		_size = size;
		_values = new float[size];
		foreach (int i in _values) {
			_values [i] = initValue;
		}
	}

	public float Filter(float v) {
		_ndx++;
		if (_ndx == _values.Length)
			_ndx = 0;
		
		_values [_ndx] = v;
		float total = 0;
		foreach (float p in _values) {
			total += p;
		}
		_lastValue = (float)total / _size;
		return _lastValue;
	}


}
