﻿using System.Collections;
using UnityEngine;

//Same as an averageFilter, but for a vector2.

public class AverageVector2Filter {

	private Vector2[] _values;
	private int _ndx = 0;
	private int _size;


	public Vector2 _lastValue;
	

	public AverageVector2Filter(int size, Vector2 initValue) {
		_size = size;
		_values = new Vector2[size];
		for(int i = 0; i < _values.Length; i++) {
			_values [i] = initValue;
		}
	}

	public Vector2 Filter(Vector2 v) {
		_ndx++;
		if (_ndx == _values.Length)
			_ndx = 0;
		
		_values [_ndx] = v;
		Vector2 total = Vector2.zero;
		foreach (Vector2 p in _values) {
			total += p;
		}
		_lastValue = (Vector2)total / _size;
		return _lastValue;
	}


}
