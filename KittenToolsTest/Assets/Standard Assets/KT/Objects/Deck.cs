﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

/* A collection of objects that you can "get" from, which never runs out. It is always random.
Note that you may get a repeat if the deck reshuffles -- so if you have 1, 2, and 3 in a deck,
and you've requested 4 items, and you got 3,1,2, for the first three items, note that the fourth 
item requested may be a 2 again, because it reshuffles the deck before it gives it to you.
Decks need tobe initted. 
*/

[System.Serializable]
public class Deck<T> {

	private T[] _deck;
	private int _ndx = 0;
	private bool _initted;
	[HideInInspector] public int Count = 0;
	

	public void Init(T[] deck) {
		_deck = deck;
		_initted = true;
		ShuffleDeck ();
		Count = _deck.Length;
	}

	public void Init(List<T> deck) {
		Init(deck.ToArray());
	}

	public T Get() {
		if (!_initted) {
			DBug.print("DECK: Gotten without initialization. No go.");
			return default(T);
		}

		if (_ndx == _deck.Length)
			ShuffleDeck();
		
		T returned = _deck [_ndx];
		_ndx++;
		return returned;

	}

	private void ShuffleDeck() {
		_deck = CollectionUtils.Shuffle<T> (_deck);
		_ndx = 0;
	}



}
