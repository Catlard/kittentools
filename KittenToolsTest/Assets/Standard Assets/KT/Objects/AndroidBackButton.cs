﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MEC;

//A useful class for managing the android back button

public enum BackButtonEventType {NONE, PAUSER, PREV_SCENE, CLOSE_MODAL}

public class AndroidBackButton : SingletonMonoBehavior<AndroidBackButton> {

    public bool _debug;
    public List<BackButtonEvent> _history;
    private string _tag;

    public void Init() {
        _tag = StringUtils.MakeTag("anbb");
		Timing.RunCoroutine(CheckEveryFrame(), _tag);
        Clear();
    }  

    public void Clear() {
        _history = new List<BackButtonEvent>();
        _history.Add(new BackButtonEvent(null, BackButtonEventType.NONE));
    }  

    public void Set(BackButtonEvent e) {
        _history.Add(e);
        if(_debug)
            print("BACK BUTTON: ASSIGNED: Event type = " + e._type);
    }


    public void Unset() {
        if(_history.Count < 2) {
            // Debug.LogError("Tried to unset back button functionality, but there was nothing to unset.");
            return;
        }
        _history.RemoveAt(_history.Count-1);
    }

    public IEnumerator<float> CheckEveryFrame() {
        while(true) {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                BackButtonEvent current = _history[_history.Count-1];
                if(current._type != BackButtonEventType.NONE) {
                    if(_debug) print("BACK BUTTON: Performed event type " + current._type);
                    current._event.Invoke();
                }
            }
            yield return Timing.WaitForOneFrame;
        }
    }


    
}

[System.Serializable]
public class BackButtonEvent {
    public UnityEvent _event;
    public BackButtonEventType _type;
    public BackButtonEvent (UnityEvent e, BackButtonEventType t) {
        _event = e;
        _type = t;
    }

}
