﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/* 
A 2d array that has special functions for getting data from certain types of positions.
It's good for iterating, getting edge positions, getting corners, getting the neighbors of
an index, etc. Very useful for grid operations!
*/



[System.Serializable]
public class Fancy2DArrayOfValues<T> where T : struct {
    public T[,] _arr;
    public readonly int _xD;
    public readonly int _yD;

    // Constructors

    public Fancy2DArrayOfValues(Vector2Int dims) {
        _arr = new T[dims.x,dims.y];
        _xD = dims.x;
        _yD = dims.y;
    }

    public Fancy2DArrayOfValues(int xD, int yD) {
        _arr = new T[xD,yD];
        _xD = xD;
        _yD = yD;
    }

    // Dimensions

    public Vector2Int GetDimensions() {
        return new Vector2Int(
            _arr.GetLength(0),
            _arr.GetLength(1)
        );
    }

    // Get Elements

    public T this[int x, int y]
    {
        get { return _arr[x, y]; }
        set { _arr[x, y] = value; }
    }

    public T this[Vector2Int p]
    {
        get { return _arr[p.x, p.y]; }
        set { _arr[p.x, p.y] = value; }
    }   


    public T GetMiddle() {
        int mid_x = Mathf.RoundToInt((float) _xD/2);
        int mid_y = Mathf.RoundToInt((float) _yD/2);
        return _arr[mid_x, mid_y];
    }

    // Get Positions

    public Vector2Int GetRandomPosition() {
        return new Vector2Int(UnityEngine.Random.Range(0,_xD), UnityEngine.Random.Range(0,_yD));
    }

    // Position locations

    public bool IsPositionOnEdge(Vector2Int position) {
        var result = (    position.x == 0 || position.x == _arr.GetLength(0)-1 
                    || position.y == 0 || position.y == _arr.GetLength(1)-1 );            
        return result;
    }

    public bool IsPositionOnCorner(Vector2Int position) {          
        int max_x = _arr.GetLength(0)-1;
        int max_y = _arr.GetLength(1)-1;        
        int x = position.x;         
        int y = position.y;
        var result = ((x == 0 && y == 0) || (x == max_x && y == 0) || (y == max_y && x == 0) || (x == max_x && y == max_y));
        return result;
    }        

    public bool IsPositionOnBottomEdge(Vector2Int position) {
        return position.y == 0;
    }

    public bool IsPositionOnTopEdge(Vector2Int position) {
        return position.y == _arr.GetLength(1)-1;
    }

    public bool IsPositionOnLeftEdge(Vector2Int position) {
        return position.x == 0;
    }        

    public bool IsPositionOnRightEdge(Vector2Int position) {
        return position.x == _arr.GetLength(0)-1;
    }        

    // Iterators
    // all expensive as they create an additional list

    public void EachEdgeElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnEdge(new Vector2Int(x,y));
        });
    }

        public void EachNonEdgeElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return !IsPositionOnEdge(new Vector2Int(x,y));
        });
    }

    public void EachTopElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnTopEdge(new Vector2Int(x,y));
        });
    }

    public void EachBottomElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnBottomEdge(new Vector2Int(x,y));
        });
    }

    public void EachRightElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnRightEdge(new Vector2Int(x,y));
        });
    }

    public void EachLeftElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnLeftEdge(new Vector2Int(x,y));
        });
    }

    public void EachElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action);
    }


    public void EachElementHelper(Action<T, Vector2Int, int, int> action, Func<T, int, int, bool> selector = null) 
    {
        
        List<T> enumeratables = new List<T>();
        List<int> xs = new List<int>();
        List<int> ys = new List<int>();
        List<Vector2Int> ps = new List<Vector2Int>();

        for (int x = 0; x < _arr.GetLength(0); x++) {
            for (int y = 0; y < _arr.GetLength(1); y++) {
                if (selector == null || selector.Invoke(_arr[x,y], x, y)) {
                    enumeratables.Add(_arr[x,y]);
                    xs.Add(x);
                    ys.Add(y);
                    ps.Add(new Vector2Int(x,y));
                }
            }
        }
        
        var i = 0;
        foreach (var e in enumeratables) {
            action(e, ps[i], xs[i], ys[i]);
            i++;
        }
    }          

    public string Dump() {
        string map = "Dumped fancy2dArray (" + _xD + "x" + _yD + "):\n";
        for (int y = _arr.GetLength(1)-1; y >=0; y--) {
            for (int x = 0; x < _arr.GetLength(0); x++) {
                map += _arr[x, y].ToString();                
            }
            map += "\n";
        }
        return map;
    }  

}


[System.Serializable]
public class Fancy2DArray<T>  where T: class {
    public T[,] _arr;
    public readonly int _xD;
    public readonly int _yD;
    public int _midX;
    public int _midY;

    // Constructors

    void DetermineMiddle() {
        _midX = Mathf.RoundToInt((float) _xD/2);
        _midY = Mathf.RoundToInt((float) _yD/2);
    }

    public Fancy2DArray(Vector2Int dims) {
        _arr = new T[dims.x,dims.y];
        _xD = dims.x;
        _yD = dims.y;
        DetermineMiddle();
    }

    public Fancy2DArray(int xD, int yD) {
        _arr = new T[xD,yD];
        _xD = xD;
        _yD = yD;
        DetermineMiddle();
    }

    // Dimensions

    public Vector2Int GetDimensions() {
        return new Vector2Int(
            _arr.GetLength(0),
            _arr.GetLength(1)
        );
    }

    // Get Elements

    public T this[int x, int y]
    {
        get { return _arr[x, y]; }
        set { _arr[x, y] = value; }
    }

    public T this[Vector2Int p]
    {
        get { return _arr[p.x, p.y]; }
        set { _arr[p.x, p.y] = value; }
    }   

    public Neighbors<T> GetCardinalNeighbors(Vector2Int pos) {
        return new Neighbors<T>(this, pos);
    }

    public EightWayNeighbors<T> GetNeighbors(Vector2Int pos) {
        return new EightWayNeighbors<T>(this, pos);
    }    

    public T GetCenterElement() {
        return this[GetCenterPosition()];
    }

    // Get Positions

    public Vector2Int GetCenterPosition() {
        int mid_x = Mathf.FloorToInt((float) (_xD-1)/2);
        int mid_y = Mathf.FloorToInt((float) (_yD-1)/2);
        return new Vector2Int(mid_x,mid_y);
    }

    public Vector2Int GetRandomPosition() {
        return new Vector2Int(UnityEngine.Random.Range(0,_xD), UnityEngine.Random.Range(0,_yD));
    }

    public Vector2Int? GetRandomEmptyAdjacentPosition(Vector2Int pos) {
        var possiblePositions = new List<Vector2Int>();
        if (!IsPositionOnLeftEdge(pos) && _arr[pos.x-1, pos.y] == null) {
            possiblePositions.Add(new Vector2Int(pos.x-1, pos.y));
        }
        if (!IsPositionOnRightEdge(pos) && _arr[pos.x+1, pos.y] == null) {
            possiblePositions.Add(new Vector2Int(pos.x+1, pos.y));
        }
        if (!IsPositionOnBottomEdge(pos) && _arr[pos.x, pos.y-1] == null) {
            possiblePositions.Add(new Vector2Int(pos.x, pos.y-1));
        }        
        if (!IsPositionOnTopEdge(pos) && _arr[pos.x, pos.y+1] == null) {
            possiblePositions.Add(new Vector2Int(pos.x, pos.y+1));
        }                    
        
        if (possiblePositions.Count == 0)
            return null;
        else
            return possiblePositions[UnityEngine.Random.Range(0,possiblePositions.Count)];
    }

    // Position locations

    public bool IsPositionOnEdge(Vector2Int position) {
        var result = (    position.x == 0 || position.x == _arr.GetLength(0)-1 
                    || position.y == 0 || position.y == _arr.GetLength(1)-1 );            
        return result;
    }

    public bool IsPositionOnCorner(Vector2Int position) {          
        int max_x = _arr.GetLength(0)-1;
        int max_y = _arr.GetLength(1)-1;        
        int x = position.x;         
        int y = position.y;
        var result = ((x == 0 && y == 0) || (x == max_x && y == 0) || (y == max_y && x == 0) || (x == max_x && y == max_y));
        return result;
    }        

    public bool IsPositionOnBottomEdge(Vector2Int position) {
        return position.y == 0;
    }

    public bool IsPositionOnTopEdge(Vector2Int position) {
        return position.y == _arr.GetLength(1)-1;
    }

    public bool IsPositionOnLeftEdge(Vector2Int position) {
        return position.x == 0;
    }        

    public bool IsPositionOnRightEdge(Vector2Int position) {
        return position.x == _arr.GetLength(0)-1;
    }        

    // Iterators
    // all expensive as they create an additional list

    public void EachEdgeElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnEdge(new Vector2Int(x,y));
        });
    }

        public void EachNonEdgeElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return !IsPositionOnEdge(new Vector2Int(x,y));
        });
    }

    public void EachTopElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnTopEdge(new Vector2Int(x,y));
        });
    }

    public void EachBottomElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnBottomEdge(new Vector2Int(x,y));
        });
    }

    public void EachRightElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnRightEdge(new Vector2Int(x,y));
        });
    }

    public void EachLeftElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action, (obj, x, y) => {
            return IsPositionOnLeftEdge(new Vector2Int(x,y));
        });
    }

    public void EachElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action);
    }

    public void EachNonNullElement(Action<T, Vector2Int, int, int> action) {
        EachElementHelper(action,  (obj, x, y) => {
            return obj != null;
        });
    }

    public void EachElementHelper(Action<T, Vector2Int, int, int> action, Func<T, int, int, bool> selector = null) 
    {
        
        List<T> enumeratables = new List<T>();
        List<int> xs = new List<int>();
        List<int> ys = new List<int>();
        List<Vector2Int> ps = new List<Vector2Int>();

        for (int x = 0; x < _arr.GetLength(0); x++) {
            for (int y = 0; y < _arr.GetLength(1); y++) {
                if (selector == null || selector.Invoke(_arr[x,y], x, y)) {
                    enumeratables.Add(_arr[x,y]);
                    xs.Add(x);
                    ys.Add(y);
                    ps.Add(new Vector2Int(x,y));
                }
            }
        }
        
        var i = 0;
        foreach (var e in enumeratables) {
            action(e, ps[i], xs[i], ys[i]);
            i++;
        }
    }    

    public List<T> ToFlattenedNonNullList() 
    {
        
        List<T> flatGuy = new List<T>();

        for (int x = 0; x < _arr.GetLength(0); x++) {
            for (int y = 0; y < _arr.GetLength(1); y++) {
                if (_arr[x,y] != null) {
                    flatGuy.Add(_arr[x,y]);                       
                }
            }
        }
        
        return flatGuy;
    }            

    public string Dump() {
        string map = "Dumped fancy2dArray (" + _xD + "x" + _yD + "):\n";
        for (int y = _arr.GetLength(1)-1; y >=0; y--) {
            for (int x = 0; x < _arr.GetLength(0); x++) {
                if (_arr[x,y] != null)
                    map += _arr[x, y].ToString();
                else
                    map += "_";
            }
            map += "\n";
        }
        return map;
    }  

}
