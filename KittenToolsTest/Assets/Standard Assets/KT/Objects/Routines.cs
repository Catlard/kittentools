﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Essentially, a facade for the RoutineManager. It maintains a 
reference to a RoutineManager, so if you intend to use KTBehaviors
and Routines.GetTag to manage all your async operations, then
you need to Init or assign a new _routines value. Note that
this class can be reset, and that the RoutineManager can be reset.

The most useful function here is GetTag, which gives you a unique
string that is preregistered with the RoutineManager. You can then
use this string as an ID for MEC or DoTween operations, and be
confident that those operations will be killed with the object
automatically.

*/

public static class Routines {

	//initialize here, so can run functions even if app hasn't started up and assigned this yet.
	public static RoutineManager _routines = new RoutineManager("NONE", false);

	//startup
	public static void Init() {
		Reset();
		InitPauseables();
	}

	//new scene? do this.
	public static void Reset() {
		Nuke();
		_routines = new RoutineManager("reset" + TimeUtils.GetUnixTime(), false);
	}

	//the nuclear option
	public static void Nuke() { 
		_routines.KillAll(); 
		_routines.Clear(); 
	}

	//register/unregister routines attached to gameobjects, or with tags. 
	public static void UnregisterAll(MonoBehaviour m) { _routines.UnregisterAll(m);}
	public static void Unregister(string s) { _routines.Unregister(s);}
	public static string GetTag(MonoBehaviour m, string prefix) {
		int p;
		if(string.IsNullOrEmpty(prefix)) 
			Debug.LogWarning("You are registering an empty tag for go " + StringUtils.GetHierarchyString(m.gameObject.transform) + ". That seems wrong, but OK...");
		else if(int.TryParse(prefix, out p)) 
			Debug.LogWarning("You are registering a tag which parses as an int, " + p + ". Be warned that this may collide with a gameobject instance ID, since those are also ints.");

		return _routines.Register(m, prefix);
	}
	public static void RemoveTag(string tag) {_routines.Unregister(tag);}


	//things you can do with routines
	public static void Kill(string tag) {_routines.Kill(tag);}
	public static void Pause(string tag) {_routines.Pause(tag);}
	public static void Resume(string tag) {_routines.Resume(tag);}
	public static void Kill(MonoBehaviour m) {_routines.Kill(m);}
	public static void Pause(MonoBehaviour m) {_routines.Pause(m);}
	public static void Resume(MonoBehaviour m) {_routines.Resume(m);}
	public static void KillAll() {_routines.KillAll();}
	public static void PauseAll() {_routines.PauseAll();}
	public static void ResumeAll() {_routines.ResumeAll();}

	public static void InitPauseables() {
		PauseableRigidbody2D[] rs = GameObject.FindObjectsOfType<PauseableRigidbody2D>();
		foreach(PauseableRigidbody2D r in rs) {r.Init();}

		PauseableParticleSystem[] pss = GameObject.FindObjectsOfType<PauseableParticleSystem>();
		foreach(PauseableParticleSystem ps in pss) {ps.Init();}

		PauseableAnimator[] ans = GameObject.FindObjectsOfType<PauseableAnimator>();
		foreach(PauseableAnimator an in ans) {an.Init();}
	}

}
