﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


/* A helper class for saving screenshots to a folder on your desktop. Used for release testing, in my case. */

public class ScreenshotHelper : MonoBehaviour
{
    // Start is called before the first frame update
    string desktopPath;
    public string _sceneName;

    void Start() {
        desktopPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "/";
        Debug.LogWarning("WARNING: Screenshot helper is on. This should not be for builds. Saving to: " + desktopPath);
    }

    public void MakeScreenshot(string dir, string imageName, int magnification = 6) {
        dir = desktopPath + dir;
        if (!Directory.Exists(dir)) {
            Directory.CreateDirectory(dir);
        }

        string fileName = dir + imageName + ".png";
        // print("Made: " + imageName + " @ " + fileName);
        ScreenCapture.CaptureScreenshot(fileName, magnification);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.I)) {
           MakeScreenshot("", _sceneName + Random.Range(0, 100000));
        }
    }
}
