﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/*

This class performs operations on intervals, or two numbers with space in between.
A new MinMax(1,2) lerped by .5 returns 1.5f, for example.

*/

[System.Serializable]
public class MinMax {
	public float _min = 1;
	public float _max = 1;

	public bool IsInRange(float number) {
		return number >= _min && number <= _max;
	}

	public float Clamp(float value) {
		return Mathf.Clamp(value, _min, _max);
	}

	public float GetRandom(bool signed = false, bool integer = false) {
		float r = UnityEngine.Random.Range(_min, _max); 	
		if (signed) {
			r = UnityEngine.Random.value > .5f ? r : -r;
		}
		if (integer) {
			r = Mathf.RoundToInt(r);
		}

		return r;
	}

	//if the min is 0, and the max is 10, GetPercent(.5f) returns 5.
	public float GetLerped(float percent) {
		return Mathf.Lerp(_min, _max, percent);
	}

	public float PercentIn(float inRange) {
		return (inRange-_min)/(_max-_min);
	}

	public float GetDiff() {
		return _max - _min;
	}

	public float GetInclusiveDiff() {
		return _max - _min + 1;
	}

	public int GetLerpedInt(float phase) {
		return Mathf.CeilToInt(GetLerped(phase));
	}


	public int GetRandomInt() {
		return Mathf.RoundToInt(GetRandom());
	}

	public MinMax AddToRange(float amount) {
		return new MinMax (_min + amount, _max + amount);
	}

	public MinMax (float min, float max) {
		_min = min;
		_max = max;
	}

	public MinMax(float value) {
		_max = value;
		_min = value;
	}

	public MinMax(MinMax a, MinMax b) {
		MinMax intersection = a.FindIntersection(b);
		_min = intersection._min;
		_max = intersection._max;
	}
	
	public MinMax FindIntersection(MinMax other) {
    	return new MinMax(
			Math.Max(_min, other._min),
			Math.Min(_max, other._max)
		);
	}



	public override string ToString()
	{
		return "Min: " + (Mathf.Floor(_min*1000)/1000) + ", Max: " + (Mathf.Floor(_max*1000)/1000);
	}

	

}

[System.Serializable]
// MinMaxInt is inclusive of min and exclusive of max
// lenght is max - min
public class MinMaxInt {
	public int _min;
	public int _max;

	// Construct with empty interval
	public MinMaxInt () {
		_min = 0;
		_max = 0;
	}	

	public MinMaxInt (int min, int max) {
		_min = min;
		_max = max;
	}

	public MinMaxInt (MinMaxInt a, MinMaxInt b) {
		MinMaxInt intersection = a.FindIntersection(b);
		_min = intersection._min;
		_max = intersection._max;
	}	

	public bool IsEmpty() {
		return _max - _min <= 0;
	}

	public int IntervalLength() {
		if (IsEmpty()) return 0;
		return _max - _min;
	}

	public bool IsInRange(int number) {
		return number >= _min && number < _max;
	}

	public MinMaxInt FindIntersection(MinMaxInt other) {
    	return new MinMaxInt(
			Math.Max(_min, other._min),
			Math.Min(_max, other._max)
		);
	}	

	public List<int> GetValuesInInterval() {
		var returned = new List<int>();
		for (int i=_min; i<_max; i++) {
			returned.Add(i);
		}
		return returned;
	}	

	override public string ToString() {
		return _min+".."+(_max-1);
	}
}