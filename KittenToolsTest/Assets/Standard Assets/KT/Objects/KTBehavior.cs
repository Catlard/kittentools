﻿using UnityEngine;

//the base class for objects that need to be able to "pause" at any
//point in their lifetime. Routines manages these and will stop
//all coroutines/tweens related to this object, when it is destroyed.

public abstract class KTBehavior : MonoBehaviour
{
	virtual public void OnDestroy() { 
		Routines.Kill(this);
		Routines.UnregisterAll(this);
	} //remove all tags/routines related to this object.
}
