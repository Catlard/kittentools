﻿using System.Collections.Generic;
using UnityEngine;

/* A helper class that pools game objects and deactivates them when they're "recycled", rather than
destroyed. Because it's better to re-use objects rather than to keep destroying and creating them all
the time (for example -- with bullets).
*/

public class GOPool {
    
	public GameObject _prefab;
	public GameObject[] _allObjects;
	public List<GameObject> _availableObjects;
	public Transform _poolParent;


	public void Init(int numberOfObjects, GameObject prefab, Transform poolParent) {
		_availableObjects = new List<GameObject>();
		_allObjects = new GameObject[numberOfObjects];
		_prefab = prefab;

		for(int i = 0; i < numberOfObjects; i++) {
			GameObject g = Object.Instantiate (_prefab);
			_availableObjects.Add(g);
			_allObjects[i] = g;
		}
	}

	public GameObject Get() {
		if(_availableObjects.Count < 1) {
			Debug.LogError("Tried to get from object pool, but there was nothing left in the pool.");
			return null;
		} else {
			GameObject pooled = _availableObjects[0];
			_availableObjects.RemoveAt(_availableObjects.Count-1);
			pooled.SetActive(true);
			return pooled;
		}
	}

	public GameObject[] GetAll() {
		foreach(GameObject go in _availableObjects)
			go.SetActive(true);

		return _availableObjects.ToArray();
	}

	public void RecycleAll(GameObject[] toPool) {
		foreach(GameObject go in toPool) {
			Recycle(go);
		}
	}

	public void Recycle(GameObject pooled) {
		_availableObjects.Add(pooled);
		pooled.SetActive(false);
		pooled.transform.SetParent(_poolParent);
	}

	public void Destroy() {
		for(int i = 0; i < _allObjects.Length; i++) {
			Object.Destroy (_allObjects [i]);
		}
	}


}
