﻿/* A helper class for tracking time. Works like a stopwatch! Doesn't keep going when the app is suspended.*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Timer {

    public bool _isRunning = true;
    private float _elapsedTime = 0f;
    private float _lastStartTime = 0f;

    public string _name = "?";

    public override string ToString() {
        if(_isRunning)
            StoreTime();

        string state = _isRunning ? "running" : "paused"; 
        float rTime = MathUtils.RoundTo(_elapsedTime, .01f);
        return "CLOCK " + _name + ": " + state + " @ " + rTime + "s.";
    }

    private void StoreTime() {
        float toStore = Time.time - _lastStartTime;
        _lastStartTime = Time.time;
        _elapsedTime += toStore;
    }

    public void OnApplicationPause(bool isPaused) {
        if(isPaused) {
            Pause();
        } else {
            Resume();
        }
    }



    public Timer (string name) {
        Reset();
        _name = name;
    }

    public void Reset() {
        _lastStartTime = Time.time;
        _elapsedTime = 0;
        _isRunning = true;
    }

    public float GetSeconds() {
        if(_isRunning)
            StoreTime();
        return _elapsedTime;
    }

    public void Pause() {
        if(!_isRunning) return;
        StoreTime();
        _isRunning = false;
    }

    public void Resume() {
        if(_isRunning) return;
        _lastStartTime = Time.time;
        _isRunning = true;

    }

    public void AddTime(float amount) {
        _lastStartTime -= amount;
        StoreTime();
    }
	
}
