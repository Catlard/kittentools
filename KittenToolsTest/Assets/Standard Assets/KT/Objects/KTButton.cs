﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;


//A custom button class for the UnityUI library -- useful if you want specific button behaviors.

public class KTButton : MonoBehaviour,  IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler{
   
    public bool _isPressing;
    private bool _initted;

    public UnityEvent _action;


    public void Start() {
        if(_initted) return;
        _initted = true;
    }

    public void OnPointerDown (PointerEventData eventData) {
        PopDown(false);
    }
 
    public void OnPointerUp (PointerEventData eventData) {
        PopUp(false);
    }

    public void OnPointerExit (PointerEventData eventData) {
        if(_isPressing) PopUp(true);
    }

    public void OnPointerEnter (PointerEventData eventData) {
        if(_isPressing) PopDown(true);
    }

    private void PopUp(bool fromEnter) {
        if(!_initted) Start();
        if(!fromEnter)
            _isPressing = false;
    }

    private void PopDown(bool fromEnter) {
        if(!_initted) Start();
        if(!_isPressing) _action.Invoke();
        _isPressing = true;
    }
    
}
