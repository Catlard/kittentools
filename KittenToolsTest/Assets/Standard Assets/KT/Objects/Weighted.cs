﻿/* 
Functions like a weighted dice. You control the frequency with which something comes up! 
A good alternative to using a Deck, sometimes.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable] 
public class WeightedPairDictionary : SerializableDictionary<string, float> {}

[System.Serializable]
public class Weighted {

	[Inspectionary]
	public WeightedPairDictionary _weights;

	//if seeded, then always returns the same selections.
	public Weighted(string name) {
		_weights = new WeightedPairDictionary();
	}

	public float GetTotalWieght() {
		float total = 0;
		foreach(KeyValuePair<string, float> p in _weights) {
			total+=p.Value;
		}
		return total;
	}

	public int GetOptions() {
		return _weights.Count;
	}

	public void RemoveKeys(string[] remove) {
		foreach(string s in remove) {
			_weights.Remove(s);
		}
	}

	public void AddKey(string key, float value) {
		_weights.Add(key, value);
	}

	public KeyValuePair<string, float> Roll() {

		if (_weights.Count == 0) {
			Debug.LogError ("You forgot to put values in some weighted object.");
		}

		float totalWeight = 0;

		foreach (KeyValuePair<string, float> p in _weights) {
			totalWeight += p.Value;
		}

		float randomAmt = totalWeight * Random.value;


		foreach (KeyValuePair<string, float> p in _weights) {
			if (randomAmt < p.Value) {
				return p;
			}
			randomAmt -= p.Value;
		}

		//will never get here
		Debug.LogError("SHOULD NEVER GET HERE.");
		return new KeyValuePair<string, float> ();

	}


}
