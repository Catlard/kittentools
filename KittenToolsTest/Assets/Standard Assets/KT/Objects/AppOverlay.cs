﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

//A solution for overlays using the Unity UI. If you want the screen to fade to black using an image component, add this to it!

public class AppOverlay : KTBehavior
{

    private Image _ren;
    private SpriteRenderer _spriteRen;
    public Color _fullColor;
    [HideInInspector] public float _fadeTime = .5f;
    private string _tag;
    private bool _blocking = true;


    public void Init(Color fullColor, bool blocking = true)
    {
        _tag = StringUtils.MakeTag("AppOverlay");
        _ren = GetComponent<Image>();
        _spriteRen = GetComponent<SpriteRenderer>();
        _fullColor = fullColor;
        if (_spriteRen != null)
            _spriteRen.color = _fullColor;
        else
            _ren.color = _fullColor;
        Blocking(false);
    }

    public void Fade(Color c, float time)
    {
        Blocking(_blocking);
        _fullColor = c;
        if (_spriteRen != null)
            _spriteRen.DOColor(c, time).SetId(_tag);
        else
            _ren.DOColor(c, time).SetId(_tag);
    }

    public void Fade(float time)
    {
        Fade(_fullColor, time);
    }

    public void Fade(Color c)
    {
        Fade(c, _fadeTime);
    }

    public void Fade()
    {
        Fade(_fullColor, _fadeTime);
    }

    public void TurnBlocking(bool capable)
    {
        _blocking = capable;

    }

    public void Blocking(bool state = false)
    {
        // print(transform.name + " ::::: "+ state);

        if (_spriteRen == null)
        {
            _ren.raycastTarget = state;
        }
    }

    public void FadeOut(float time)
    {
        DOTween.Kill(_tag);

        Color destination = ColorUtils.Clarify(_fullColor);
        if (time == 0)
        {
            if (_spriteRen != null)
                _spriteRen.color = destination;
            else
                _ren.color = destination;
            Blocking(false);
        }
        else
        {
            if (_spriteRen != null)
                _spriteRen.DOColor(destination, time).SetId(_tag).OnComplete(delegate { Blocking(false); });
            else
                _ren.DOColor(destination, time).SetId(_tag).OnComplete(delegate { Blocking(false); });
        }
    }

    public void FadeOut()
    {
        FadeOut(_fadeTime);
    }

}
