﻿using UnityEngine;

/* put this on any RIGIDBODY 2D component in the editor, and you can pick up "pause" 
events. from the modal manager. */


public class PauseableRigidbody2D : MonoBehaviour {

  private Rigidbody2D body;
  private Vector3 savedVelocity;
  private float savedAngularVelocity;
  private bool _initted;
  private bool _pausedWhileActive = false;

  public void Awake() {
    try {Init();} catch {}
  }

  public void Init() {
    if(_initted) return;
    body = GetComponent<Rigidbody2D>();
    Routines._routines._onPause += PauseListener;
    _initted = true;
  }

  public void OnDestroy() {
    Routines._routines._onPause -= PauseListener;
  }

  private void PauseListener(bool pause) {
      if(pause) {
        if(_pausedWhileActive) return;
        if(body.simulated) _pausedWhileActive = true;

        savedVelocity = body.velocity;
        savedAngularVelocity = body.angularVelocity;
        body.simulated = false;
      } else {
        body.velocity = savedVelocity;
        body.angularVelocity = savedAngularVelocity;
        body.simulated = true;
        _pausedWhileActive = false;
      }
  }

}