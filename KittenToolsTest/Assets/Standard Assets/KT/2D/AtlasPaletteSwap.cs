﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

/* 
A script for changing all of the colors on a sprite sheet to a few different colors,
without having to store it as a PNG. You can use the "GetColors" function in the editor
behind the gear on this script to fill up the "original colors" array, then change them
as you wish in the other colors arrays!
*/

public class AtlasPaletteSwap : MonoBehaviour {

	public Sprite _anySpriteFromAtlas;
	public Color[] _originalColors;
	public Color[] _colors1;
	public Color[] _colors2;
	public Color[] _colors3;

	public List<Texture2D> _atlases;

	public Sprite[] _swappedPalettes;

	public SpriteRenderer _debugSprite;

	private void CopyPalette(ref Color[] p) {
		p = new Color[_originalColors.Length];
		for(int i = 0; i < _originalColors.Length; i++) {
			Color c = _originalColors [i];
			p [i] = new Color (c.r, c.g, c.b, c.a);
		}
	}

	private Texture2D Paint(int ndx) {
		Texture2D texToPaint = _anySpriteFromAtlas.texture;
		Color[] paletteToUse = _originalColors;
		Texture2D t = new Texture2D (texToPaint.width, texToPaint.height);
		t.name = texToPaint.name + "_" + ndx;

		if (ndx == 1)
			paletteToUse = _colors1;
		if (ndx == 2)
			paletteToUse = _colors2;
		if (ndx == 3)
			paletteToUse = _colors3;

		for (int x = 0; x < texToPaint.width; x++) {
			for(int y = 0; y < texToPaint.height; y++) {
				Color thisPixel = texToPaint.GetPixel (x, y);

				bool isPainted = false;
				for (int j = 0; j < _originalColors.Length; j++) {
					Color origColor = _originalColors [j];
					if (thisPixel == origColor) {
						t.SetPixel (x, y, paletteToUse [j]);
						isPainted = true;
					}
				}
				if (!isPainted) {
					t.SetPixel (x, y, Color.clear);
				}
			}
		}
		t.Apply ();
		return t;
	}
		

	#if UNITY_EDITOR
		[ContextMenu ("Get Original")]
		public void GetOriginalColors() {
			Rect sRect = _anySpriteFromAtlas.textureRect;
			Color[] tex = _anySpriteFromAtlas.texture.GetPixels ();
			List<Color> palette = new List<Color> ();
			for (int i = 0; i < tex.Length; i++) {
				bool found = false;
				Color thisPixel = tex [i];
				foreach (Color foundBefore in palette) {
					if (foundBefore == thisPixel) {
						found = true;
					}
				}
				if (!found && thisPixel.a > 0) {
					palette.Add (thisPixel);
				}
			}
			_originalColors = palette.ToArray ();
			Debug.Log(_originalColors.Length + " colors placed in original colors.");
		}
		

		[ContextMenu ("Reset 1")]
		public void Copy1() {
			CopyPalette (ref _colors1);
		}

		[ContextMenu ("Reset 2")]
		public void Copy2() {
			CopyPalette (ref _colors2);
		}

		[ContextMenu ("Reset 3")]
		public void Copy3() {
			CopyPalette (ref _colors3);
		}

		[ContextMenu ("Reset Colors")]
		public void ResetColors() {
			GetOriginalColors ();
			Copy1 ();
			Copy2 ();
			Copy3 ();
		}


		[ContextMenu ("-------")]
		public void Blank() {}


		private void ChangeTestSprite(int pal) {
			_debugSprite.sprite = _anySpriteFromAtlas;
			Init();
			_debugSprite.sprite = GetSwappedSprite(pal, _debugSprite.sprite);
		}


		[ContextMenu ("Show 1")] public void ResetColors1() {ChangeTestSprite(1);}
		[ContextMenu ("Show 2")] public void ResetColors2() {ChangeTestSprite(2);}
		[ContextMenu ("Show 3")] public void ResetColors3() {ChangeTestSprite(3);}

//		[ContextMenu ("Paint Orig")] public void Paint1() {  Paint (0); }
//		[ContextMenu ("Paint 1")] public void Paint1() {  Paint (1); }
//		[ContextMenu ("Paint 2")] public void Paint2() {  Paint (2); }
//		[ContextMenu ("Paint 3")] public void Paint3() {  Paint (3); }
//

	#endif


	public void Init() {
		_atlases = new List<Texture2D> ();
		_atlases.Add(Paint(0));
		_atlases.Add(Paint(1));
		_atlases.Add(Paint(2));
		_atlases.Add(Paint(3));
	}

	public Sprite GetSwappedSprite(int atlas, Sprite swapping, FilterMode mode = FilterMode.Point) {
		if(atlas == 0) return swapping;

		Rect spriteRect = swapping.textureRect;
		Texture2D t = new Texture2D ((int) spriteRect.width, (int) spriteRect.height, TextureFormat.RGBA32, false);
		t.filterMode = mode;
		
		t.SetPixels(_atlases [atlas].GetPixels((int) spriteRect.x, (int) spriteRect.y, (int) spriteRect.width, (int) spriteRect.height));
		t.Apply();
		Sprite created = Sprite.Create(t, new Rect(0,0, (int) spriteRect.width, (int) spriteRect.height), new Vector2(.5f,.5f));
		created.name = swapping.name + "_" + atlas;
		return created;
	}
}
