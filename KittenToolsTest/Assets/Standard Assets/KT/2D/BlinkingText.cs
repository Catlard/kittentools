﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;

/* A class for creating blinking text that goes on and off, a mainstay in 2D games.
For example, if you want "insert coin" text..*/
public class BlinkingText : KTBehavior {

	public float _blinkSpeed = 1;
	private string _tag;

	// Use this for initialization
	public void Init () {
		_tag = Routines.GetTag(this, "bt");
		Timing.RunCoroutine(Blink(), _tag);
	}

	private IEnumerator<float> Blink() {
		CanvasGroup g = GetComponent<CanvasGroup>();
		TMPro.TextMeshPro text = GetComponent<TMPro.TextMeshPro>();
		while(true) {
			yield return Timing.WaitForSeconds(1.5f * _blinkSpeed);
			if(g != null) g.alpha = 0;
			else text.enabled = false;
			yield return Timing.WaitForSeconds(.5f * _blinkSpeed);
			if(g != null) g.alpha = 1;
			else text.enabled = true;
		}
	}

	public void Stop() {
		Routines.Kill(this);
	}
}
