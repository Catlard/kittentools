﻿using UnityEngine;
using System.Collections.Generic;
using MEC;

/* A simpler version of AnimatedSprite which automatically loops. */

public class LoopingSprite : KTBehavior {


	public Sprite[] _showSprites;
	private int _currSpriteNdx;
	public float _fps = 3;
	private UnityEngine.UI.Image _image;
	public bool _destroyOnFinish = false;
	public bool _autoStart = false;
	[HideInInspector] public string _tag;
	
	
	[HideInInspector] public SpriteRenderer _renderer;


	public void Start() {
		if (_autoStart)
			Init ();
	}

	virtual public void Init() {
		Stop();
		_renderer = GetComponent<SpriteRenderer> ();
		_image = GetComponent<UnityEngine.UI.Image> ();
		_tag = Routines.GetTag(this, "loops");
		Timing.RunCoroutine(AnimateRoutine(), _tag);
	}

	public void Init(Sprite[] toShow) {
		_showSprites = toShow;
		Init ();
	}


	public void Stop() {
		Routines.Kill(_tag);
	}

	public void Play() {
		Timing.RunCoroutine(AnimateRoutine(), _tag);
	}

	public void Reset ()
	{
		Routines.Kill(_tag);
		_currSpriteNdx = 0;
		_renderer.sprite = _showSprites[0];
	}

	public float GetPercentFinished() {
		return (float) _currSpriteNdx/(_showSprites.Length-1);
	}

	public float GetAnimationLength() {
		return 1f / _fps * _showSprites.Length;
	}

	private IEnumerator<float> AnimateRoutine() {
		if (_fps == 0) {
			print ("WOULD HAVE ANIMATED AGAIN");
			yield break;
		}

		while (true) {
			NextFrame();
			yield return Timing.WaitForSeconds (1/_fps);
		}


	}

	virtual public void NextFrame() {
		GetNextSpriteNdx ();
		if(_renderer!=null)
			_renderer.sprite = _showSprites [_currSpriteNdx];
		if (_image != null) {
			_image.sprite = _showSprites [_currSpriteNdx];
		}
	}

	private void GetNextSpriteNdx() {
		_currSpriteNdx++;
		if (_currSpriteNdx >= _showSprites.Length) {
			_currSpriteNdx = 0;
			if (_destroyOnFinish) {
				Stop();
				GameObject.Destroy (gameObject);
			}
		}
	}

}
