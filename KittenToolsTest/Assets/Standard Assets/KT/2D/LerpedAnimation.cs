﻿using UnityEngine;
using System.Collections;

/* A base class for customizable animation controllable by a percent. Useful for 
animations with unusual looping or scaling timings.
*/

public class LerpedAnimation : MonoBehaviour {

	[HideInInspector] public SpriteRenderer _renderer;
	public Sprite[] _spritesInOrder;

	virtual public void Init() {
		_renderer = GetComponent<SpriteRenderer> ();
		Show (false);
	}
	
	
	virtual public void Show(bool state) {
		_renderer.enabled = state;
	}


	virtual public void UpdateState(float percent) {
		UpdateStateRaw(percent);
	}

	public void UpdateStateRaw(float percent) {
		int nearestNdx = Mathf.RoundToInt ((_spritesInOrder.Length - 1) * percent);
		_renderer.sprite = _spritesInOrder [nearestNdx];
	}
}
