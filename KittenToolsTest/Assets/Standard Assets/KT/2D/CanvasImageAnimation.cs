﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;

/* Basically like AnimatedSprite, but for the canvas. Animate on unity ui! */

public class CanvasImageAnimation : KTBehavior {

	[HideInInspector] public Image _image;
	public Sprite[] _frames;
	public float _fps;
	public int _loops; //-1 is forever.
	private int _currentLoopsRemaining;
	public float _delayBetweenPlays = 1;
	public bool _preserveSpriteSize = true;
	public bool _destroyOnComplete = false;
	public bool _reverse;

	public int _currentFrame;

	private float _timePerFrame;

	[HideInInspector] public RectTransform _rt;
	private float _initialMagnification;
	string _tag;

	public void Init(string name) {
		_tag = Routines.GetTag(this, "cia");
		_timePerFrame = 1f/_fps;
		_image = GetComponent<Image>();
		_rt = GetComponent<RectTransform>();
		_initialMagnification = _image.sprite.rect.width / _rt.sizeDelta.x;
		ResetLoops();
		SetFrame(0);

		// print("STARTED UNDER: " + StringUtils.GetHierarchyString(transform));
	}

	public void Play() {
		SetFrame(0);
		ResetLoops();
		Routines.Kill(_tag);
		Timing.RunCoroutine(PlayRoutine(),Segment.RealtimeUpdate, _tag);
	}

	public void ResetLoops() {
		_currentLoopsRemaining = _loops;
	}

	public void SetFrame(int frame) {
		if(_reverse) _currentFrame = _frames.Length-1-frame;
		else _currentFrame = frame;
		_image.sprite = _frames[_currentFrame];
		if(_preserveSpriteSize) {
			_rt.sizeDelta = new Vector2(_image.sprite.rect.width, _image.sprite.rect.height);
			_rt.sizeDelta /= _initialMagnification;
		}
	}

	public void Pause() {
		Routines.Kill(_tag);
	}

	public void Show(bool show) {
		_image.enabled = show;
	}

	public IEnumerator<float> PlayRoutine() {
		while(_currentLoopsRemaining != 0) {
			_currentLoopsRemaining--;
			for(int i = 0; i < _frames.Length; i++) {
				SetFrame(i);
				yield return Timing.WaitForSeconds(_timePerFrame);
			}
		}
		yield return Timing.WaitForOneFrame;
		if (_destroyOnComplete){
			Destroy(gameObject);
		}
	}
}
