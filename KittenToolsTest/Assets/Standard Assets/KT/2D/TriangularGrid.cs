﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class TriangularGridFactory
{

 /*   
--[[makes sets of coordinates that form a triangle. They can be wonky, meaning not uniformly spaced.

wonkiness = 0: note that it is a uniform triangle.

(0,0)       (1,0).      
        . 
      . . . 
    . . . . . 
  . . . . . . .
(0,1)       (1,1)

with some wonkiness:

wonkiness = something higher than 0: note that all points are offset.

         . 
      . .. 
    . . . .  . 
  .  ..  .. . .


In any case, a point is represented by a kv table, like {x = .34, y = .57}

Note that if the number of items in the triangle size requested is not a triangular number,
like 1, 3, 6, or 10, then this class will return a table with the next highest number of
positions in it. So, if you ask for a triangle for 4 objects, it will give you 6 positions.

Also note that a triangle can be centered in a grid either by its circumcenter/centroid
(the center of its vertices), or by the geometric center (half of the height/width). This
grid is centered around the centroid.

In the case of circular objects, we pack them more tightly by moving the rows together more,
into a triangular packing formation. This is an option, called useTriangularPacking

]]

*/

    public static Vector2[] Make(float size, bool useTriangularPacking = true, float wonkiness = 0) {

        List<Vector2> _grid = new List<Vector2>();

        if(size == 0) return _grid.ToArray();

        float bigEnoughTriangle = 0;
        float rows = 0;
        while(bigEnoughTriangle <= size) {
            rows = rows + 1;
            bigEnoughTriangle = (rows *(rows + 1)) / 2;
        }

        float between = 1/rows;
        float betweenY = between;
        float currX = .5f;
        float currY = between/2f;

        if(useTriangularPacking) {
            betweenY = between * .9f;
            currY = (1-(betweenY * (rows-1)))/2;
        }

        for(int rowNum = 0; rowNum < rows; rowNum++) {
            float rowX = currX;
            for (int colNum = 0; colNum < rowNum; colNum++) {
                float finalX = rowX;
                float finalY = currY;
                finalX = finalX + Mathf.Clamp(MathUtils.RandDirection() * wonkiness, 0, 1);
                finalY = finalY + Mathf.Clamp(MathUtils.RandDirection() * wonkiness, 0, 1);
                _grid.Add( new Vector2(rowX, currY));
                rowX = rowX + between;
            }
            currX -= between/2f;
            currY += betweenY;
        }

        return _grid.ToArray();
    }
}
