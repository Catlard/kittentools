﻿using UnityEngine;
using System.Collections;

public class ParallaxLayer : MonoBehaviour {

	private Vector2 _originalPosition;
	public Vector2 _movementScale = Vector3.one;

	//create a parallax effect by changing the movement scale.

	public void Init() {
		_originalPosition = new Vector2 (transform.localPosition.x, transform.localPosition.y);
	}

	/*
	 * input is the vector relating to where the perspective is. 
	 * if the vector is 0, 0 (bottom left), then the object, if it
	 * has a negative movement scale it will go to the top right.
	 * if the vector is 1, 1 then it will go to the bottom left.
	 * 
	 * a movement scale of zero is no parallax at all.
	 * 
	 * end of line.
	 */

	public void ReceiveInput(Vector2 input) {
		
		Vector2 newPos = _originalPosition + (	
			new Vector2(_movementScale.x * input.x, _movementScale.y * input.y)
		);

		transform.localPosition = new Vector3 (newPos.x, newPos.y, transform.localPosition.z);
	}


}
