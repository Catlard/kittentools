﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using TMPro;
using UnityEngine.Events;
using System;

/* makes a number that rolls up gradually, which is good for showing a changing
number for points.
*/

public class UIScoreRoller : KTBehavior {

	[HideInInspector] public TextMeshProUGUI _UItext;
	[HideInInspector] public TextMeshPro _text;
	private float _lerp;
	private float _targetScore;
	private bool _soundReady;
	private float _prevScoreShown;
	public bool _mute;
	
	[HideInInspector] public float _shownScore;

	public bool _int;
	public string _soundName;
	public string _tweenedSoundName;
	public float _timeBetweenSounds = .1f;
	public float _volume;

	private UnityAction<int, TextMeshProUGUI, TextMeshPro> _onTweenChangeAction;
	private Func<float, string> _onGetFormattedString;

	string _tag;


	public void ResetSounds() {
		_soundReady = !string.IsNullOrEmpty(_soundName);
		if(string.IsNullOrEmpty(_tweenedSoundName) && _soundReady)
			_tweenedSoundName = _soundName;
	}

	private string DefaultStringFormatter(float score){
		return score.ToString();
	}

	public void Show(bool state) {
		if(_UItext == null) {
			_text.enabled = state;
		} else {
			_UItext.enabled = state;
		}
	}

	public void Init(
			float initialScore, 
			float lerpAmount, 
			UnityAction<int, TextMeshProUGUI, TextMeshPro> onTweenChangeAction,
			Func<float, string> onGetFormattedString
	){
		_onGetFormattedString = onGetFormattedString == null ? DefaultStringFormatter : onGetFormattedString;
		_onTweenChangeAction = onTweenChangeAction;
		ResetSounds();
		_UItext = GetComponent<TextMeshProUGUI>();
		_text = GetComponent<TextMeshPro>();
		_lerp = lerpAmount;
		bool oldMute = _mute;
		_mute = true;
		SetScore(initialScore);
		_mute = oldMute;
		_tag = Routines.GetTag(this, "uisr");
		
	}
	
	private IEnumerator<float> PlaySound(float score) {
		_soundReady = false;
		string soundName = _targetScore == score ? _tweenedSoundName : _soundName;
		if(!_mute)
			SoundLibrary.instance.PlaySound(new SoundParams(soundName, 0, _volume));
		yield return Timing.WaitForSeconds(_timeBetweenSounds);
		_soundReady = true;
	}

	private void ShowScore(float score) {
		if(_int) score = Mathf.RoundToInt(score);
		
		if(_soundReady && _prevScoreShown != score){
			Timing.RunCoroutine(PlaySound(score), _tag);
			if (_onTweenChangeAction != null) {
				_onTweenChangeAction.Invoke((int) score, _UItext, _text);
			}
		}

		string formatted = _onGetFormattedString.Invoke(score);
		if (_text != null){
			_text.text = formatted;
		} else {
			_UItext.text = formatted;
		}

		_prevScoreShown = score;
	}

	public void TweenTo(float destination) {
		Routines.Kill(_tag);
		ResetSounds();
		_targetScore = destination;
		Timing.RunCoroutine(Do(), _tag);
	}

	public void SetScore(float newScore, Color c) {
		SetScore(newScore);
		if (_UItext == null)
			_text.color = c;
		else
			_UItext.color = c;
	}

	public void SetScore(float newScore) {
		Routines.Kill(_tag);
		ResetSounds();
		_targetScore = newScore;
		_shownScore = _targetScore;
		ShowScore(_targetScore);
	}

	private IEnumerator<float> Do() {
		while(true) {
			//DBug.print(_shownScore, _targetScore, _lerp);
			_shownScore = Mathf.Lerp(_shownScore, _targetScore, _lerp);
			ShowScore(_shownScore);
			yield return Timing.WaitForOneFrame;
		}
	}

	
}
