﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

/* Another effect I use all over the place with Unity UI -- for when you want a button to slide off the screen, 
and be untouchable, and then come back later and be available to the user. */

public class ButtonHider {

    public RectTransform _buttonRT;
    public Vector2 _hidePosition;
    public Vector2 _showPosition;
    private string _tag;
    [HideInInspector] public float _time = .25f;
    private Ease _showEase = Ease.OutBack;
    private Ease _hideEase = Ease.InBack;
    private Button _butt;


    public bool _overshoot = true;
    public bool _throb = false;

    private void Init(RectTransform buttonRT, Vector2 hideOffset, MonoBehaviour origin, bool startHidden = true) {
        // Debug.LogWarning("FOUND: " + StringUtils.GetHierarchyString(_buttonRT));
        _buttonRT = buttonRT;
        if(_buttonRT == null) {
            Debug.LogError("ButtonHider Couldn't find an object titled: " + buttonRT.gameObject.name);
        }
        _showPosition = _buttonRT.anchoredPosition;
        _hidePosition = _showPosition + hideOffset;
        _tag = Routines.GetTag(origin, buttonRT.gameObject.name.Substring(0, 5));
        _butt = _buttonRT.GetComponent<Button>();
        Show(!startHidden, 0, true);
    }

    public ButtonHider(RectTransform buttonRT, Vector2 hideOffset, MonoBehaviour origin, bool startHidden = true) {
        Init(buttonRT, hideOffset, origin, startHidden);
    }

    public ButtonHider(string buttonGOName, Vector2 hideOffset, MonoBehaviour origin, bool startHidden = true) {        
        Init(GameObject.Find(buttonGOName).GetComponent<RectTransform>(), hideOffset, origin, startHidden);
    }

    public void Show(bool state, float delay = 0, bool immediately = false) {
        // Debug.Log(_buttonRT.transform.name + " --> " + state);
        Routines.Kill(_tag);
        Vector2 destination = state ? _showPosition : _hidePosition;

        if(immediately) {
            _buttonRT.anchoredPosition = destination;
            if(_butt != null)
                _butt.interactable = state;
            return;
        }

        Ease ease = state ? _showEase : _hideEase;
        bool inTheMiddle = _buttonRT.anchoredPosition != _showPosition &&
                            _buttonRT.anchoredPosition != _hidePosition;
        if(inTheMiddle || !_overshoot) {
            ease = state ? Ease.OutCubic : Ease.InCubic;
        }

        if(!state && _butt != null) {
            _butt.interactable = false;
        }

        if(!state || !_throb) {
            _buttonRT.DOScale(Vector3.one, .5f).SetId(_tag).SetEase(Ease.InOutCubic);
        }

        _buttonRT.DOAnchorPos(destination, _time)
            .SetEase(ease)
            .SetDelay(delay)
            .OnComplete(() => {
                if(_throb && state) {
                    _buttonRT.DOScale(Vector3.one * 1.1f, .5f)
                        .SetDelay(2)
                        .SetId(_tag)
                        .SetEase(Ease.InOutCubic)
                        .SetLoops(5, LoopType.Yoyo);
                }

                if(state && _butt != null)
                    _butt.interactable = true;
            })
            .SetId(_tag);
    }


}
