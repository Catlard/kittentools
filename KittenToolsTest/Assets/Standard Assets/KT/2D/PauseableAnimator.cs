﻿using UnityEngine;

/* put this on any ANIMATOR component in the editor, and you can pick up "pause" 
events. from the modal manager. */


public class PauseableAnimator : MonoBehaviour {

  private Animator anim;
  private bool _initted;
  private float _previousSpeed;

  public void Awake() {
    try {Init();} catch {}
  }

  public void Init() {
    if(_initted) return;
    anim = GetComponent<Animator>();
    Routines._routines._onPause += PauseListener;
    _initted = true; //needs to beafter. only happens when init succeeds.
  }

  public void OnDestroy() {
    Routines._routines._onPause -= PauseListener;
  }

  private void PauseListener(bool pause) {
      //string name = StringUtils.GetHierarchyString(transform);
      if(pause) {
        _previousSpeed = anim.speed;
        anim.speed = 0;
        // print("PAUSED ON " + name);
      } else {
        anim.speed = _previousSpeed;
        // print("RESUMED: " + name);
      }
  }

}