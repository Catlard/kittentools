﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;

/* 
this is a way of keeping and accessing large numbers of stored sprites. 
the basic workflow is -- put a spritesheet in your resources folder, load it in
with the helper function AddFromResources, and then you can gather sprite coll-
ections (or single sprites) by using the GetSprites... functions. 
*/

public class SpriteHolder : MonoBehaviour {

	public Sprite[] _allSprites;
//	public Sprite[] _found;

	virtual public Sprite[] GetSpritesWithPattern(string pattern) {
		List<Sprite> returned = new List<Sprite> ();
		foreach (Sprite s in _allSprites) {
			if (Regex.IsMatch (s.name, pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase)) {
				returned.Add (s);
			}
		}

		return returned.ToArray();
	}

	public Sprite[] GetSpritesWithPatterns(string[] patterns) {
		Sprite[] returned = new Sprite[0];
		foreach(string s in patterns) {
			Sprite[] f = GetSpritesWithPattern (s);
			returned = returned.Concat(f).ToArray();
		}
		return returned;
	}

	virtual public Sprite GetSprite(string name) {
		for(int i = 0; i < _allSprites.Length; i++) {
			if(_allSprites[i].name == name) {
				return _allSprites[i];
			}
		}
		print("No sprite named " + name + " in holder.");
		return null;
	}

	public Texture2D GetSpriteAsTexture2D(string name) {
		Sprite found = GetSprite(name);
		if(found == null) 
			return null;
		else
			return PixelUtils.Tex2DFromSprite(found);
	}

	public void AddFromResources(string sheetInResources) {
		if(_allSprites == null) 
			_allSprites = new Sprite[0];
		Sprite[] newSprites = Resources.LoadAll<Sprite>(sheetInResources);
		Debug.Assert(newSprites.Length > 0, "Sprite sheet "+sheetInResources+"does not exist, or has no sprites");
		List<Sprite> joined = new List<Sprite>();
		foreach(Sprite s in _allSprites) joined.Add(s);
		foreach(Sprite s in newSprites) joined.Add(s);
		_allSprites = joined.ToArray();
	}

}
