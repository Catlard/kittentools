﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//a collection parallax layers which work together.

public class ParallaxStack : MonoBehaviour {

	public AnimationCurve _edgeCurve = AnimationCurve.Linear(0,0,1,1); //controls how the parallax reacts to being at the edges -- can make it slow down gradually as it gets to the end of the range.
	public Vector2 _middle = new Vector2(.5f,.5f);
	private List<ParallaxLayer> _layers;
	public float _lerpAmount = .5f;

	private Vector2 _targetPercent;
	private Vector2 _currentPercent;

	private bool _initted = false;

	public void Init() {

		if (_initted)
			return;

		_initted = true;

		_layers = new List<ParallaxLayer> ();
		foreach (Transform c in transform) {
			ParallaxLayer p = c.GetComponent<ParallaxLayer> ();
			if (p != null) {
				_layers.Add (p);
				p.Init ();
			}
		}


		UpdateStack (new Vector2(.5f, .5f));
	}

	public void UpdateStack(Vector2 screenPercent) {

		_targetPercent = screenPercent;

		Vector2 v = Vector2.Lerp( _currentPercent, _targetPercent,  _lerpAmount);
		_currentPercent = v;

		v = new Vector2 (
            _edgeCurve.Evaluate (v.x), 
            _edgeCurve.Evaluate (v.y)
        );

		v -= _middle;


		foreach (ParallaxLayer p in _layers) {
			p.ReceiveInput (v);
		}

	}

	public void ModifyLayerVectors(Vector2 mult) {
		foreach (ParallaxLayer l in _layers) {
			l._movementScale.Scale(mult);
		}
	}
}
