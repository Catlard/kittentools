﻿using UnityEngine;

/* put this on any PARTICLE SYTEM component in the editor, and you can pick up "pause" 
events. from the modal manager. */

public class PauseableParticleSystem : MonoBehaviour {

  private ParticleSystem system;
  private bool _initted;
  private bool _pausedWhileActive;

  public void Awake() {
    try {Init();} catch {}
  }

  public void Init() {
    if(_initted) return;
    system = GetComponent<ParticleSystem>();
    Routines._routines._onPause += PauseListener;
    _initted = true; //needs to beafter. only happens when init succeeds.
  }

  public void OnDestroy() {
    Routines._routines._onPause -= PauseListener;
  }

  private void PauseListener(bool pause) {
      if(pause) {
        if(_pausedWhileActive) return;
        if(system.isPlaying) _pausedWhileActive = true;
        system.Pause(true);
      } else if(!pause && _pausedWhileActive) {
        system.Play(true);
        _pausedWhileActive = false;
      }
  }

}