﻿using UnityEngine;
using System.Collections.Generic;
using MEC;

/* 

	A class for creating frame by frame sprite animation. 
	You must init before you play.

*/

public class AnimatedSprite : KTBehavior {

	public Sprite[] _showSprites;
	private int _currSpriteNdx;
	public float _fps = 3; //starts as 3, but if -666 then stay whatever was last assigned
    public int _loops;
    public bool _isAnimating;
	[HideInInspector] public string _tag;
	[HideInInspector] public SpriteRenderer _renderer;

	public void Init() {
		_renderer = GetComponent<SpriteRenderer> ();
		_tag = Routines.GetTag(this, "loops");
	}

	public void Stop() {
		Routines.Kill(_tag);
	}

	public void Play(Sprite[] sprites, int loops = 1, float customFPS = -666) {
		_fps = customFPS == -666 ? _fps : customFPS;
        _showSprites = sprites;
        _currSpriteNdx = 0;
        _loops = loops;
        Stop();
		Timing.RunCoroutine(PlayRoutine(), _tag);
	}

	public void Play(int loops = 1, float customFPS = -666) {
		Play(_showSprites, loops, customFPS);
	}

	public void Play(List<Sprite> sprites, int loops = 1, float customFPS = -666) {
		Play(sprites.ToArray(), loops, customFPS);
	}

	//todo make it work for loops. ew. broken currently.
	public float GetPercentFinished() {
		return (float) _currSpriteNdx/(_showSprites.Length-1);
	}

	public float GetTimeRemainingInAnimation() {
		return _loops / _fps * _showSprites.Length ;
	}

	private IEnumerator<float> PlayRoutine() {
        _isAnimating = true;

		NextFrame(); //start on the first frame, or else madness

		// print("WAITING, WILL WAIT: " + (1f/_currentFPS));
		while (_isAnimating) {
            while(_fps == 0) yield return Timing.WaitForOneFrame;
            yield return Timing.WaitForSeconds (1f/_fps);
			
			_isAnimating = NextFrame();
		}
	}

	public bool NextFrame() {
		
		if (_currSpriteNdx >= _showSprites.Length) {
			_currSpriteNdx = 0;
			_loops--;
            if(_loops == 0) {
                return false;
            } 
		} 
        _renderer.sprite = _showSprites [_currSpriteNdx];
		_currSpriteNdx++;
        return true;
	}

}
