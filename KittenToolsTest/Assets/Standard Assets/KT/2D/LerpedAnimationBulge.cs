﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class LerpedAnimationBulge : LerpedAnimation {

	public float _bulgeAmount = 1.2f;
	public float _bulgeTime = .5f;

	private float _startScale;
	private bool _wasBulging = false;


	public override void Init ()
	{
		_startScale = transform.localScale.x;
		base.Init ();
	}

	override public void Show (bool state)
	{
		StopBulge ();
		base.Show (state);
	}

	private void StopBulge() {
		StopCoroutine ("Bulge");

		transform.localScale = Vector3.one * _startScale;
	}


	private IEnumerator Bulge() {
		while (true) {
			transform.DOScale (_startScale * _bulgeAmount, _bulgeTime).SetEase(Ease.InOutQuad);
			yield return new WaitForSeconds (_bulgeTime);
			transform.DOScale (_startScale, _bulgeTime).SetEase(Ease.InOutQuad);
			yield return new WaitForSeconds (_bulgeTime);
		}
	}

	public override void UpdateState (float percent)
	{
		if (percent == 1 && !_wasBulging) {
			StopBulge ();
			StartCoroutine ("Bulge");
			_wasBulging = true;
		} else if (percent < 1 && _wasBulging) {
			StopBulge ();
			_wasBulging = false;
		}

		base.UpdateState (percent);
	}



}
