﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class RectangularGridFactory
{

 /*   
    extents is the size, dims is the number of rows (y) and columns (x)
    given Make(new Vector2(14, 6), new Vector2Int(7, 3)); it displays something like this:

    . . . . . . .
    . . . . . . .
    . . . . . . .
*/

    public static Vector2[] Make(Vector2 extents, Vector2Int dims) {

        if(dims.x == 0 || dims.y == 0) {
            Debug.LogError("No 0 coords allowed in rectangular grids, you silly billy!");
        }

        Vector2[] grid = new Vector2[dims.x * dims.y];
        int ndx = 0;
		for(int x = 0; x < dims.x; x++) {
            float xPhase = dims.x == 1 ? .5f : (float) x / (dims.x - 1);
			for(int y = 0; y < dims.y; y++) {
                float yPhase = dims.y == 1 ? .5f : (float) y / (dims.y-1);
                grid[ndx] = new Vector2(
                    Mathf.Lerp(-extents.x, extents.x, xPhase),
                    Mathf.Lerp(-extents.y, extents.y, yPhase)
                );
                ndx++;
			}
		}
        return grid;
    }
}
