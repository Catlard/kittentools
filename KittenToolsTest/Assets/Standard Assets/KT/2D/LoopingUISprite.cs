﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* A version of LoopingSprite for the Unity UI */

public class LoopingUISprite : LoopingSprite {

	RectTransform _rt;
	float _sizeMult;
	Image _imageRenderer;


	//assumes aspect ratio/size at start is proper aspect ratio and multiple

	override public void Init() {
		_rt = GetComponent<RectTransform>();
		_imageRenderer = GetComponent<Image>();
		_sizeMult = _rt.sizeDelta.x / GetComponent<Image>().sprite.rect.width;
		print(_sizeMult);

		base.Init();
	}

	override public void NextFrame() {
		base.NextFrame();
		_rt.sizeDelta = new Vector2(
			_imageRenderer.sprite.rect.width,
			_imageRenderer.sprite.rect.height
		) * _sizeMult;
	}
}
