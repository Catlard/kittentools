﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* 

This is a class which makes a singleton monobehavior that inherits
from Monobehavior. So, declare one like this:

public class MySingleton : SingletonMonoBehavior<MySingleton> {}

And then access your singleton instance like this: MySingleton.instance.DoSomePublicFunction();

Note that creating more than one of a singleton behavior will not cause an error -- but only
one of them will run. So be careful! I use this class for creating MVC structures -- so the
model, controller, and view can talk to each other easily.

*/

public abstract class SingletonMonoBehavior<T> : MonoBehaviour
{

	private static T _instance;

	public virtual void Awake() {
		if(_instance == null || _instance.Equals(default(T)))
			_instance = (T)((System.Object)this);
	}

	public static T instance {
		get { return _instance; }
	}

}