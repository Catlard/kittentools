﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* 

This is a class which makes a singleton monobehavior that inherits
from KTBehavior. So, declare one like this:

public class MySingleton : SingletonKTBehavior<MySingleton> {}

And then access your singleton instance like this: MySingleton.instance.DoSomePublicFunction();

Note that creating more than one of a singleton behavior will not cause an error -- but only
one of them will run. So be careful! I use this class for creating MVC structures -- so the
model, controller, and view can talk to each other easily.

*/

public abstract class SingletonKTBehavior<T> : KTBehavior
{

	private static T _instance;
	private static Dictionary<string, bool> _waitConditions = new Dictionary<string, bool>();
	public static bool _debugWaiting;

	private static void LogIt(object msg) {
			// print("SKT: " + msg.ToString());
	}

	public virtual void Awake() {
		if(_instance == null || _instance.Equals(default(T)))
			_instance = (T)((System.Object)this);
	}

	public static T instance {
		get { return _instance; }
	}

	public static bool WaitingFor(string conditionName) {
		return _waitConditions.ContainsKey(conditionName);
	}

	public static void StartWaitingFor(string conditionName) {
		if(WaitingFor(conditionName)) {
			// LogIt("Started waiting more than once for the same condition, " + conditionName);
		} else {
			_waitConditions.Add(conditionName, true);
		}
	}

	public static void Done( string conditionName) {
		//print("DONE: " + conditionName);
		// LogIt("Done: " + conditionName);

		if(!_waitConditions.ContainsKey(conditionName)) { // error
			// LogIt("Tried to DONE for a condition that did not exist, " + conditionName);
		} else {
			_waitConditions.Remove(conditionName); //let them get out of the wait loop!
		}
	}

	

	

}