﻿using UnityEngine;
using System.Collections;
using System.Linq;
#if UNITY_EDITOR
	using UnityEditor;
#endif

//Useful for animations and stuff in the editor that uses the Sound Library. A helper class for using the SoundLibrary!

public class SoundTrigger : MonoBehaviour {

	#if UNITY_EDITOR
		[MenuItem("KT/SoundTriggerPitchTo_1")]
		public static void SoundTriggerPitchTo_1()
		{
			print("SETTING ALL PITCHES FOR ITEMS IN SOUND TRIGGERS IN SCENE TO 1.");
			SoundTrigger[] allTriggers = UnityEngine.Object.FindObjectsOfType<SoundTrigger>();
			print("FOUND " + allTriggers.Length + " TRIGGERS IN SCENE.");
			foreach(SoundTrigger t in allTriggers) {
				string name = StringUtils.GetHierarchyString(t.gameObject.transform);
				Debug.LogWarning("On obj " + name + " had " + t._soundParams.Length + " sounds. Fixing.");
				foreach(SoundParams p in t._soundParams) {
					p._pitch = 1;
				}			
			}

		}
	#endif

	public float _volumeMult = 1;
	public SoundParams[] _soundParams;


	public void PlaySound(string name) {
		foreach (SoundParams p in _soundParams) {
			if (p._name == name) {
				SoundParams sp = new SoundParams(p._pitch, p._volume * _volumeMult, p._name,  p._loops);
				sp._isMusic = p._isMusic;
				SoundLibrary.instance.PlaySound (sp);
			}
		}
	}

}
