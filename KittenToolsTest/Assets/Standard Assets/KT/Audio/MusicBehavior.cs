﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* base class for all musicbehaviors. */

public abstract class MusicBehavior : KTBehavior {

	virtual public void StartMusic(Dictionary<string, float> volumes) {}
	virtual public void Init() {}
	virtual public void ReceiveMessage(object message) {}
	virtual public void StopMusic() {}

//	virtual public void PitchTo(float newPitch) {}
//	virtual public void VolumeTo(float newVolume) {}

}
