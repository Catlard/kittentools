﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
#if UNITY_EDITOR
	using UnityEditor;
#endif
using System.Linq;
using MEC;


/* 

	This is the class that I use to do heavy lifting with audio. It controls the life cycle of audio sources,
	creating and destroying them for you as needed. The basic use case for this class is as follows:

	1) Add a sound library with some audio clips you'll play in the editor.
	2) Access this class and initialize it like: SoundLibrary.instance.Init();
	3) Play a sound quickly by saying SoundLibrary.instance.PlaySound("soundFileName", 0), or in a more
		customized way by using a SoundParams object. These could use a revamp, but work fine for now.
		Normally sounds are audiosources added to the same script as the SoundLibrary, to prevent any
		problems with the source being too far away from the listener. This is good for 2D sounds...
	4) If you want to control a sound that is in the process of being played, then use the PlayingSound
		object which is returned from the PlaySound functions in this class. The sounds can be paused
		stopped, or tweened as needed.

*/


public class SoundLibrary : SingletonKTBehavior<SoundLibrary> {
	#if UNITY_EDITOR
		[ContextMenu ("Sort Sounds by Name")]
		public void SortSounds() {
			if (_allSounds == null)
				return;
			_allSounds = _allSounds.Where(x => x).ToList();
			_allSounds = _allSounds.OrderBy(x => x.name).ToList();   
			Debug.Log(_allSounds.Count + " sounds have been sorted alphabetically.");
		}
	#endif

	public List<AudioClip> _allSounds;
	[HideInInspector] public List<PlayingSound> _playingSounds;
	private AudioListener _listener;
	public bool _mute;
	public bool _debugSounds;
	[Range(0f, 1f)] public float _globalVolume = 1;
	private SoundParams _params;
 	[HideInInspector] public bool _hasMusic; //quick reference, so we don't do a bunch of db lookups.

	private void Log(object msg) {
		if(_debugSounds) print("SOUNDS: " + msg.ToString());
	}

	public void Init() {
		_hasMusic = DB.GetPref<bool>(DBKey.SETTINGS_MUSIC_ON, true);
		if(_mute) _globalVolume = 0;
		_listener = Camera.main.GetComponent<AudioListener> ();
		if(GameObject.FindObjectsOfType<AudioListener>().Length == 0)
			_listener = Camera.main.gameObject.AddComponent<AudioListener> ();
		_playingSounds = new List<PlayingSound> ();
		if(Routines._routines == null || Routines._routines._onPause == null) {
			// Debug.LogWarning("Sound library couldn't find pause callback. Normally this doesn't happen. You might be in a test scene.");
		} else {
			Routines._routines._onPause += PauseListener;
		}
	}
	public void OnDestroy() {
		Routines._routines._onPause -= PauseListener;
	}

	public bool AddSound(AudioClip clip) {
		if(_allSounds.Contains(clip)) {
			Log("Tried to add " + clip.name + ", but already had it.");
			return false;
		} else {
			_allSounds.Add(clip);
			return true;
		}
	}
	
	public void PauseListener(bool pause) {
		if(pause) {
			foreach(PlayingSound ps in _playingSounds) ps.Pause();
		} else {
			foreach(PlayingSound ps in _playingSounds) ps.Resume();
		}
	}



	public AudioClip FindClip(string name) {
		foreach (AudioClip clip in _allSounds) {
			if (clip != null && clip.name == name) {
				return clip;
			}
		}

		if(_debugSounds)
	 		Debug.LogError("SOUND LIBRARY: No clip called " + name);
		return null;
	}

	public PlayingSound PlaySound(string name, float pitchVariance) {
		return PlaySound(new SoundParams(name, pitchVariance));
	}

	public PlayingSound PlaySound(string[] choices, float pitchVariance) {
		return PlaySound(new SoundParams(choices[Random.Range(0, choices.Length)], pitchVariance));
	}

	public PlayingSound PlaySound(SoundParams p) {
		Log("PLAYING SOUND: " + p._name);
		AudioClip thisClip = FindClip(p._name);
		AudioSource l = _listener.gameObject.AddComponent<AudioSource> ();	
		SoundParams pCopy = new SoundParams (p._pitch, p._volume, p._name, p._loops, p._isMusic);
		PlayingSound mySound = new PlayingSound (pCopy, l, thisClip,  _globalVolume);
		mySound.Start();
		_playingSounds.Add(mySound);
		return mySound;
	}

	public void KillSoundImmediately(PlayingSound s) {
		// print(s._clip.name + " was destroyed");
		Destroy (s._source);
		_playingSounds.Remove (s);
	}

	public void SetGlobalVolume(float percent) {
		if(_mute) _globalVolume = 0;
		_globalVolume = percent;
		foreach (PlayingSound s in _playingSounds) {
			s.AdjustForNewGlobalVolume(percent);
		}
	}

	public void TweenGlobalVolume(float percent, float duration) {
		if(_mute) percent = 0;
		foreach (PlayingSound s in _playingSounds) {
			s.TweenForNewGlobalVolume(percent, duration);
		}
	}

	public PlayingSound GetPlayingSound(string name) {
		foreach (PlayingSound s in _playingSounds) {
			if (s._source.clip.name == name)
				return s;
		}
		return null;
	}

	public bool IsPlayingSound(string name) {
		PlayingSound s = GetPlayingSound (name);
		return s == null ? true : false;
	}
		
}


[System.Serializable]
public class SoundParams {
	public string _name;
	public float _pitch = 1f;
	public float _volume = 1f;
	public int _loops = 1;
	public bool _isMusic;

	public SoundParams (string name, float pitchVariance = 0, float volume = 1, int loops = 1) {
		_name = name; 
		_pitch = 1 + ((Random.value * pitchVariance) * (Random.value > .5f ? 1 : -1));
		_volume = volume; _loops = loops;
	}

	public SoundParams (float pitch, float volume, string name, int loops = 1, bool isMusic = false) {
		_name = name; _pitch = pitch; _volume = volume; _loops = loops; _isMusic = isMusic;
	}

	public override string ToString() {
		return string.Format("Sound params for {0}: VO:{1}, PI:{2}, LOO:{3}", _name, _volume, _pitch, _loops);
	}
}

[System.Serializable] 
public class PlayingSound {
	public SoundParams _soundParams;
	public AudioSource _source;
	public string _tag;
	public string _volumeTag;
	public AudioClip _clip;
	private bool _isPaused;
	private float _globalVolume;
	public bool _isFinished = false;
	

	private float GetCurrentVolume() {
		if(!SoundLibrary.instance._hasMusic && _soundParams._isMusic) {
			Debug.LogWarning("Muted music: " + _soundParams._name + ".");
			return 0;
		} else {
			return _globalVolume * _soundParams._volume;
		}
	}

	public PlayingSound(SoundParams p, AudioSource s, AudioClip clip, float globalVolume) {
		_soundParams = p; _source = s; _globalVolume = globalVolume; _clip = clip;
		_tag = Routines.GetTag(SoundLibrary.instance,_soundParams._name);
		_volumeTag = Routines.GetTag(SoundLibrary.instance,_soundParams._name + "_vol");
	}

	//warning not threadsafe.
	public void AdjustForNewGlobalVolume(float newGlobalVolume) {
		_globalVolume = newGlobalVolume;
		_source.volume = GetCurrentVolume();
	}
		
	public void SetVolume(float newVolume) {
		// _soundParams._volume = newVolume;
		_source.volume = newVolume; //GetCurrentVolume();
	}

	public void SetPitch(float newPitch) {
		// _soundParams._pitch = newPitch;
		_source.pitch = newPitch; //_globalVolume * _soundParams._pitch;
	}

	public void TweenForNewGlobalVolume(float newGlobalVolume, float duration) {
		DOTween.Kill(_volumeTag);
		_globalVolume = newGlobalVolume;
		_source.DOFade (newGlobalVolume, duration)
			.SetId(_volumeTag);
	}

	public void Start() {
		_source.clip = _clip;
		_source.volume = GetCurrentVolume();
		_source.pitch = _soundParams._pitch;
		if(_source.pitch <= 0) {
			Debug.LogError("Source played with <=0 pitch. Weird. Setting to 1 for: " + _source.clip.name);
			_source.pitch = 1;
		}
		Timing.RunCoroutine(PlaySoundRoutine(), _tag);
	}

	public void Kill() {
		if(_source != null)
			_source.Stop();
		_isFinished = true;
		DOTween.Kill(_volumeTag);
		Timing.KillCoroutines(_tag);
		SoundLibrary.instance.KillSoundImmediately(this);
	}

	private IEnumerator<float> PlaySoundRoutine() {

		if(_source.clip == null)
			yield break;

		

		while(true) {
			if(_soundParams._loops == 0) {
				_isFinished = true;
				// Debug.Log (_clip.name + " ++ " + _soundParams._loops);
				SoundLibrary.instance.KillSoundImmediately(this);
				yield break;
			}
			_soundParams._loops--;
			
			_source.Play ();

			float time = _source.clip.length;
			
			while(time > 0) {
				yield return Timing.WaitForOneFrame;
				time -= Time.deltaTime * _source.pitch;
			}
		}
	}

	public void Pause() {
		if(_isPaused) return;
		_isPaused = true;
		Routines.Pause(_tag);
		Routines.Pause(_volumeTag);
		_source.Pause();
	}

	public void Resume() {
		if(!_isPaused) return;
		_isPaused = false;
		Routines.Resume(_tag);
		Routines.Resume(_volumeTag);
		_source.Play();
	}

	public void TweenVolume(float volumePercent = 1, float duration = 1, bool killOnComplete = false) {
		float actualVolume = volumePercent * GetCurrentVolume();
		//  Debug.Log("TWEEN VOL: " + _source.clip.name + " to " + actualVolume);
		if(duration == 0) {
			_source.volume = actualVolume;
		} else {
			_source.DOFade (actualVolume, duration)
				.SetId(_volumeTag)
				.OnComplete(() => {
					if(killOnComplete) {
						
					}
				});
		}
		
	}

	public void TweenPitch(float pitchPercent = 1, float duration = 1) {
		float actualPitch = pitchPercent * _soundParams._pitch;
		 //Debug.Log("PIT: " + _source.clip.name);
		_source.DOPitch (actualPitch, duration)
			.SetId(_tag);
	}

}
