﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* 
this class works with the SoundLibrary to play
randomized music. to make this work properly, you need to export
your music as "stems" -- one file per instrument track.
then this file plays them all at the same time, fading instruments
in and out to make it sound like endless music, looping dynamically.
*/

public class MusicBehavior_Stems : MusicBehavior {

	public float _volume;
	public float _volumeChangeTime = 4f;
	public string[] _stems;
	private float[] _targetVolumes;
	public int _loops = -1;
	[HideInInspector] public float _startingPitch = 1f;

	private float _globalVolume = 1;

	public string _state = "off";


	public PlayingSound[] _playingSounds;


	public override void Init ()
	{
		_targetVolumes = new float[_stems.Length];
		_playingSounds = new PlayingSound[_stems.Length];

		base.Init ();
	}

	public void PlayTrack(string name) {
		for (int i = 0; i < _playingSounds.Length; i++) {
			if (_playingSounds [i]._soundParams._name == name) {
				_targetVolumes [i] = _volume * _globalVolume;
			}
		}
		UpdateTrackVolumes ();
	}

	public void ChangePlayingTracks(int stemsToPlay) {
		//print("PLAYING STEMS: " + stemsToPlay + " on " + transform.gameObject);
		int[] randomNdxes = new int[_stems.Length];
		for (int i = 0; i < _stems.Length; i++) {
			randomNdxes [i] = i;
			_targetVolumes [i] = 0; // assume all is silent.
		}
		randomNdxes = CollectionUtils.Shuffle<int> (randomNdxes);

		for (int j = 0; j < stemsToPlay; j++) {
			_targetVolumes [randomNdxes [j]] = _volume * _globalVolume;
		}

		UpdateTrackVolumes ();
	}

	//go through and TWEEN SOUNDS TO NEW VOLUMES.
	public void UpdateTrackVolumes() {
		// string msg = "";
		for (int k = 0; k < _targetVolumes.Length; k++) {
			// msg += _targetVolumes[k] + " ... " + _playingSounds[k]._clip.name + "\n";
			if(_playingSounds[k]._source != null)
				_playingSounds [k].TweenVolume(_targetVolumes[k], _volumeChangeTime); 
		}
		// Debug.LogWarning(msg);
	}

	public void SetGlobalVolume(float newGlobalVolume) {
		//print("SET GLOBAL VOL TO : " + newGlobalVolume);
		if(newGlobalVolume == 0)
			newGlobalVolume = .001f;

		float mult =  newGlobalVolume / _globalVolume;
		_globalVolume = newGlobalVolume;

		for(int i = 0; i < _targetVolumes.Length; i++) {
			_targetVolumes[i] *= mult;
		}
		UpdateTrackVolumes();
	}

	public override void StartMusic (Dictionary<string, float> volumes) //note that this does not cause music to play. change the number of playing tracks in the above function.
	{
		volumes = volumes == null ? 
			new Dictionary<string, float>() : volumes;

		if (_state == "playing") {
			print ("MUSICBEHAVIOR: Can't play while playing.");
			return;
		}
		_state = "playing";
		for (int i = 0; i < _stems.Length; i++) {
			float trackVolume = volumes.ContainsKey(_stems[i]) ? volumes[_stems[i]] : 1;
			SoundParams musicParams = new SoundParams (1, _volume * trackVolume, _stems [i], _loops, true);
			PlayingSound s = SoundLibrary.instance.PlaySound (musicParams);
			s._source.volume = 0;
			_playingSounds [i] = s;
		}
		base.StartMusic (volumes);
	}

	public override void StopMusic() {
		if (_state == "off") {
			print ("MUSICBEHAVIOR: Can't stop while off.");
			return;
		}
		_state = "off";
		for (int i = _playingSounds.Length - 1; i >= 0; i--) {
			_playingSounds [i].Kill();
		}
		_playingSounds = new PlayingSound[_stems.Length];
		_targetVolumes = new float[_stems.Length];
		base.StopMusic ();
	}

	public void PitchTo (float newPitch, float duration)
	{
		foreach (PlayingSound p in _playingSounds) {
			p.TweenPitch(newPitch, duration);
		}
	}

	public void VolumeTo (float newVolume, float duration)
	{
		foreach (PlayingSound p in _playingSounds) {
			p.TweenVolume( newVolume, duration);
		}
	}




}
