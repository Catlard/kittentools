﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using MEC;

/* 

	Given a bunch of sound effects, plays them randomly in the background, as music.
	Works in much the same way as MusicBehavior_Stems, but assumes that the
	audioclips are very short.

*/

public class MusicBehavior_Ambiance : MusicBehavior {


	[HideInInspector] public PlayingSound[] _playingSounds;
	private string _state = "off";

	public MinMax _delayRange;
	public MinMax _volumeRange;
	public int _numberOfAmbiancesToRandomlyUse = 10;
	public AudioClip[] _clipNames;
	private float _volumeMult = 1;
	private string _tag;

	public override void Init ()
	{
		_tag = Routines.GetTag(this, "amb");

		_clipNames = CollectionUtils.Shuffle<AudioClip> (_clipNames);
		int numToUse = _numberOfAmbiancesToRandomlyUse > _clipNames.Length ? _clipNames.Length : _numberOfAmbiancesToRandomlyUse;
		Array.Resize<AudioClip> (ref _clipNames, numToUse);
		base.Init ();
	}


	public override void StartMusic (Dictionary<string, float> volumes) //note that this does not cause music to play. change the number of playing tracks in the above function.
	{
		if (_state == "on") {
			print ("MUSICBEHAVIOR: Can't start while on.");
			return;
		}
		_state = "on";

		// float _currDelayMin = _delayRange._min;
		// float _currVolumeMin = _volumeRange._min;
		float percentStep = (float)1 / _clipNames.Length;
		float currPercent = 0;
		foreach (AudioClip a in _clipNames) {
			currPercent += percentStep;
			MinMax thisDelay = new MinMax (_delayRange.GetLerped (currPercent), _delayRange._max);
			MinMax thisVolume = new MinMax (_volumeRange.GetLerped (currPercent), _volumeRange._max);
			Timing.RunCoroutine(AmbienceRoutine(new object[]{
				thisDelay, //delay
				thisVolume, //volume
				a.name, // sound

			}), _tag);
		}

		base.StartMusic (volumes);
	}

	public IEnumerator<float> AmbienceRoutine(object[] parms) {
		MinMax delayRange = (MinMax)parms [0];
		MinMax volumeRange = (MinMax)parms [1];
		string soundName = (string)parms [2];

		yield return Timing.WaitForSeconds (UnityEngine.Random.value * delayRange._max);
		while (true) {
			SoundLibrary.instance.PlaySound (new SoundParams (soundName, .2f, volumeRange.GetRandom () * _volumeMult));
			yield return Timing.WaitForSeconds (delayRange.GetRandom ());
		}

	}

	public override void StopMusic() {
		if (_state == "off") {
			print ("MUSICBEHAVIOR: Can't stop while off.");
			return;
		}
		_state = "off";

		for (int i = _playingSounds.Length - 1; i >= 0; i--) {
			SoundLibrary.instance.KillSoundImmediately (_playingSounds [i]);
		}
		Routines.Kill(_tag);
		_playingSounds = new PlayingSound[0];
		base.StopMusic ();
	}

	public void SetVolumeMult(float percent) {
		_volumeMult = percent;
	}




}




