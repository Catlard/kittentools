﻿using UnityEngine;
using System.Collections;

/*

	Has an upgraded print function which can handle multiple arguments. Useful in some situations.

*/

public static class DBug {

	private static string GetTime() {
		return MathUtils.RoundTo (Time.time, .001f).ToString () + ": ";
	}

	private static string Convert(object m) {
		return " + " + m.ToString ();
	}

	public static void print(object message) {
		Debug.Log(GetTime()  + Convert(message));
	}

	public static void print(object message, object message2) {
		Debug.Log(GetTime()  + Convert(message) + Convert(message2));
	}

	public static void print(object message, object message2, object message3) {
		Debug.Log(GetTime()  + Convert(message) + Convert(message2) + Convert(message3));
	}

	public static void print(object message, object message2, object message3, object message4) {
		Debug.Log(GetTime()  + Convert(message) + Convert(message2) + Convert(message3) + Convert(message4));
	}

	public static void print(object message, object message2, object message3, object message4, object message5) {
		Debug.Log(GetTime()  + Convert(message) + Convert(message2) + Convert(message3) + Convert(message4) + Convert(message5));
	}
}
