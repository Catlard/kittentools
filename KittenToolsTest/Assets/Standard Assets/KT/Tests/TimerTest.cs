﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerTest : MonoBehaviour {

	// Use this for initialization
	IEnumerator Start () {
		Timer t = new Timer("test");
		print(t);
		yield return new WaitForSeconds(1);
		print(t);
		t.Pause();
		print("Paused");
		yield return new WaitForSeconds(.5f);
		print(t);
		t.Resume();
		print("resumed...");
		yield return new WaitForSeconds(.5f);
		print(t);
		print("RESET:");

		t.Reset();
		print(t);
		yield return new WaitForSeconds(.5f);
		print(t);
		t.Pause();
		t.Reset();
		yield return new WaitForSeconds(.5f);
		print(t);
		
	}
	
	
}
