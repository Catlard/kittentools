﻿using UnityEngine;
using System.Collections;
using System;

public static class TypeUtils  {

	//convenience parsing function. works for all parseable types.
	public static T Parse<T>(this string source)
	{            
		if (typeof(T).IsSubclassOf(typeof(Enum)))
		{
			return (T)Enum.Parse(typeof(T), source, true);
		}

		if (!String.IsNullOrEmpty(source))
			return (T)Convert.ChangeType(source, typeof(T));

		return default(T);
	}

}
