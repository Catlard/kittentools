﻿using UnityEngine;
using System.Collections;

public static class CameraUtils {

	public static Bounds OrthographicBounds(this Camera camera)
	{
		float screenAspect = (float)Screen.width / (float)Screen.height;
		float cameraHeight = camera.orthographicSize * 2;
		Bounds bounds = new Bounds(
			camera.transform.position,
			new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
		return bounds;
	}

	public static Bounds GetPerspectiveBounds(Camera cam, float dist) {
		Vector3 bl = cam.ViewportToWorldPoint(new Vector3(0,0, dist));
		Vector3 tl = cam.ViewportToWorldPoint(new Vector3(0,1, dist));
		Vector3 br = cam.ViewportToWorldPoint(new Vector3(1,0, dist));
		Vector3 tr = cam.ViewportToWorldPoint(new Vector3(1,1, dist));
		Vector3 center = Vector3.Lerp(bl, tr, .5f);
		Bounds b = new Bounds(center, new Vector3(tr.x - tl.x, tr.y - br.y, 0));
		return b;

	}


	//if you have an object and a perspective camera, find out at what distance the object fits on screen.
	//this takes both height into account, and takes the better of the two calculations.
	public static float FindDistanceForFittingHeightOnScreen(Camera camera, Bounds objectToBeOnScreen, float percentSmallerThanFullScreen = 1) {
		float height = (objectToBeOnScreen.extents.y * 2) / percentSmallerThanFullScreen;
		float distance = height * 0.5f / Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
		return distance;

	}

	public static float FindDistanceForFittingWidthOnScreen(Camera camera, Bounds objectToBeOnScreen, float percentSmallerThanFullScreen = 1) {
		
		Bounds halfBounds = new Bounds(objectToBeOnScreen.center, objectToBeOnScreen.size * .5f);

		float widthDesired = (objectToBeOnScreen.extents.x * 2) / percentSmallerThanFullScreen;
		
		float heightDistance = FindDistanceForFittingHeightOnScreen(camera, objectToBeOnScreen, percentSmallerThanFullScreen);
		float halfHeightDistance = FindDistanceForFittingHeightOnScreen(camera, halfBounds, percentSmallerThanFullScreen);
		
		float widthAtHeightDistance = heightDistance * camera.aspect;
		float widthAtHalfHeightDistance = halfHeightDistance * camera.aspect;
		
		float lerp = Mathf.InverseLerp(widthAtHalfHeightDistance, widthAtHeightDistance, widthDesired);
		float distance = Mathf.Lerp(halfHeightDistance, heightDistance, lerp);

		return distance;
	}

	public static Vector3[] GetWorldCorners(RectTransform rt) {
		Vector3[] corners = new Vector3[4];
		rt.GetWorldCorners(corners);
		return corners;
	}

	public static Vector2 MouseClickToWorldPos(Vector2 mPos) {

		Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(mPos.x, mPos.y, 0));
		return new Vector2(pos.x, pos.y);
	}

	//screen percent to screen dimensions.
	public static Vector2 FromScreenPercent(Bounds camBounds, Vector2 mPercent) {
		return new Vector2(mPercent.x * camBounds.extents.x * 2, mPercent.y * camBounds.extents.y * 2);
	}

	//screen dimensions (Input.mousePosition) to screen percent.
	public static Vector2 ToScreenPercent(Vector2 mPos) {
		return new Vector2(mPos.x/Screen.width, mPos.y/Screen.height);

	}

}
