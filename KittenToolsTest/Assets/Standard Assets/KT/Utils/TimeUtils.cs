﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using MEC;

public static class TimeUtils {


	public static long GetUnixTime()
	{
		var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
		return (long)timeSpan.TotalSeconds;
	}

	public static DateTime UnixTimeStampToDateTime( long unixTimeStamp )
	{
		// Unix timestamp is seconds past epoch
		DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
		dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToLocalTime();
		return dtDateTime;
	}

	public static string GetReadableTimestamp(long unixTime, int hourOffset, int roundAmt = 1 ) {
		string returned = "";


		DateTime localTime =  UnixTimeStampToDateTime (unixTime).ToUniversalTime().AddHours (hourOffset);
		//gives the date in the form yymmddhh


		int hour = localTime.Hour;
		hour = Mathf.FloorToInt ((float)hour / roundAmt) * roundAmt;

		returned += StringUtils.ZFillNumber ((localTime.Year - 2000), "0", 2);
		returned += StringUtils.ZFillNumber (localTime.Month,         "0", 2);
		returned += StringUtils.ZFillNumber (localTime.Day,           "0", 2);
		returned += StringUtils.ZFillNumber (hour,                    "0", 2);

		return returned;
	}

	public static bool IsDifferentUnixDay(long unixTimeA, long unixTimeB) {
		return false;
	}

	public static float GetDuration(float startTime, float accuracy = 100) {
		float dur = Time.realtimeSinceStartup - startTime;
		dur = Mathf.Ceil(dur * accuracy) / accuracy;
		return dur;
	}

	public static IEnumerator<float> DoUpdate(System.Action func, KTBehavior scr) {
        yield return Timing.WaitForOneFrame;
		GameObject go = scr.gameObject;
        while(go != null) {
			if (scr.gameObject.activeInHierarchy && scr.enabled) func();
			yield return Timing.WaitForOneFrame;		
		}
	}

	public static IEnumerator<float> DoSlowUpdate(System.Action func, KTBehavior scr) {
        yield return Timing.WaitForOneFrame;
		GameObject go = scr.gameObject;
		float delay = Timing.Instance.TimeBetweenSlowUpdateCalls;
        while(go != null) {
			if (scr.gameObject.activeInHierarchy && scr.enabled) func();
			yield return Timing.WaitForSeconds(delay);		
		}
	}
	
}
