﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public static class GridUtils {

    public static float GetDiagonalDistance(int x1, int y1, int x2, int y2) {
        float rise = Mathf.Abs(y1 - y2);
        float run = Mathf.Abs(x1 - x2);
        float diff = Mathf.Abs(rise - run);
        float inCommon = Mathf.Max(rise, run) - diff;
        return (diff * 10) + (inCommon * 14);
    }


    public static List<(int,int)>GetSurroundingCoordinates(int col, int row, int gridWidth, int gridHeight) {
        var coords = new List<(int,int)>();
        bool leftSide = col == 0;
        bool rightSide = col == gridWidth-1;
        bool topSide = row == gridHeight-1;
        bool bottomSide = row == 0;

        if(!leftSide) {
            if(!bottomSide) coords.Add((col-1, row-1));
            if(!topSide) coords.Add((col-1, row+1));
            coords.Add((col-1, row));
        } 
        if(!rightSide) {
            if(!bottomSide) coords.Add((col+1, row-1));
            if(!topSide) coords.Add((col+1, row+1));
            coords.Add((col+1, row));
        }
        if(!bottomSide) coords.Add((col, row-1));
        if(!topSide) coords.Add((col, row+1));

        return coords;
    }

    public static Fancy2DArrayOfValues<Vector2> MakePositionGrid(Vector2Int dims, Vector2 unitSize) {

        var returned = new Fancy2DArrayOfValues<Vector2>(dims);

        var size = new Vector3((dims.x-1) * unitSize.x, (dims.y-1) * unitSize.y, 0);
        var bounds = new Bounds(Vector3.zero, size);


        if(dims.x == 1 && dims.y == 1) {
            returned[0,0] = bounds.center;
            return returned;
        }
    
        for(int x = 0; x < dims.x; x++) {
            for (int y = 0; y < dims.y; y++) {
                returned[x, y] = new Vector2(
                    Mathf.Lerp(bounds.min.x, bounds.max.x, (float) x/(dims.x-1)),
                    Mathf.Lerp(bounds.min.y, bounds.max.y, (float) y/(dims.y-1))
                );
            }
        }

        return returned;
    }

}

public enum Side {TOP, BOTTOM, LEFT, RIGHT};

public enum EightWaySide {TOP, BOTTOM, LEFT, RIGHT, TOP_RIGHT, TOP_LEFT, BOTTOM_RIGHT, BOTTOM_LEFT};

public enum OptionalSide {UP = 1, DOWN = 2, LEFT = 3, RIGHT = 4, NONE = 5};

public class Neighbors<T> where T: class {
    
    private Dictionary<Side,T> _neighbors;
    
    public Neighbors() {
        _neighbors = new Dictionary<Side, T>();
    }

    public Neighbors(Fancy2DArray<T> grid, Vector2Int pos) {
        var x = pos.x;
        var y = pos.y;
        var max_x = grid._xD-1;
        var max_y = grid._yD-1;
        _neighbors = new Dictionary<Side, T>();
        _neighbors.Add(Side.BOTTOM, y > 0 ? grid[x,y-1] : null);
        _neighbors.Add(Side.TOP, y < max_y ? grid[x,y+1] : null);
        _neighbors.Add(Side.LEFT,  x > 0 ? grid[x-1,y] : null);
        _neighbors.Add(Side.RIGHT, x < max_x ? grid[x+1,y] : null);
    }

    public T Get(Side side) {
        return _neighbors[side];
    }

    public void Set(Side side, T value) {
        _neighbors[side] = value;
    }

    public List<(Side,T)> GetNonNullSideNeighborPairs() {
        return _neighbors.Where(kvp => kvp.Value != null).Select(kvp => (kvp.Key, kvp.Value) ).ToList();
    }

    public List<T> GetNonNullNeighbors() {
        return _neighbors.Where(kvp => kvp.Value != null).Select(kvp => kvp.Value).ToList();
    }

    public T GetRandomNeighbor() {
        return _neighbors.Values.ToList()[UnityEngine.Random.Range(0,_neighbors.Values.Count)];
    }

    public override string ToString() {
        return (
            " Top: "  + (Get(Side.TOP) != null ? Get(Side.TOP).ToString() : "null")
            + " Bottom: "    + (Get(Side.BOTTOM) != null ? Get(Side.BOTTOM).ToString() : "null")
            + " Left: "  + (Get(Side.LEFT) != null ? Get(Side.LEFT).ToString() : "null")
            + " Right: " + (Get(Side.RIGHT) != null ? Get(Side.RIGHT).ToString() : "null")
        );
    }
}

public class EightWayNeighbors<T> where T: class {
    
    private Dictionary<EightWaySide,T> _neighbors;
    
    public EightWayNeighbors() {
        _neighbors = new Dictionary<EightWaySide, T>();
    }

    public EightWayNeighbors(Fancy2DArray<T> grid, Vector2Int pos) {
        var x = pos.x;
        var y = pos.y;
        var max_x = grid._xD-1;
        var max_y = grid._yD-1;
        _neighbors = new Dictionary<EightWaySide, T>();
        _neighbors.Add(EightWaySide.BOTTOM, y > 0 ? grid[x,y-1] : null);
        _neighbors.Add(EightWaySide.TOP, y < max_y ? grid[x,y+1] : null);
        _neighbors.Add(EightWaySide.LEFT,  x > 0 ? grid[x-1,y] : null);
        _neighbors.Add(EightWaySide.RIGHT, x < max_x ? grid[x+1,y] : null);

        _neighbors.Add(EightWaySide.TOP_RIGHT, x < max_x && y < max_y ? grid[x+1,y+1] : null);
        _neighbors.Add(EightWaySide.TOP_LEFT, x > 0 && y < max_y ? grid[x-1,y+1] : null);
        _neighbors.Add(EightWaySide.BOTTOM_RIGHT, x < max_x && y > 0 ? grid[x+1,y-1] : null);
        _neighbors.Add(EightWaySide.BOTTOM_LEFT, x > 0 && y > 0 ? grid[x-1,y-1] : null);
    }

    public T Get(EightWaySide side) {
        return _neighbors[side];
    }

    public void Set(EightWaySide side, T value) {
        _neighbors[side] = value;
    }

    public List<(EightWaySide,T)> GetNonNullSideNeighborPairs() {
        return _neighbors.Where(kvp => kvp.Value != null).Select(kvp => (kvp.Key, kvp.Value) ).ToList();
    }

    public List<T> GetNonNullNeighbors() {
        return _neighbors.Where(kvp => kvp.Value != null).Select(kvp => kvp.Value).ToList();
    }

    public List<T> GetNonNullNeighborsForCorner(string rotationSuffix) {
        var returned = new List<T>();
        if (rotationSuffix == "◣") {
            returned.AddRange(new List<T>() {
                _neighbors[EightWaySide.LEFT], 
                _neighbors[EightWaySide.BOTTOM_LEFT], 
                _neighbors[EightWaySide.BOTTOM]});
        } else if (rotationSuffix == "◢") {
            returned.AddRange(new List<T>() {
                _neighbors[EightWaySide.BOTTOM], 
                _neighbors[EightWaySide.BOTTOM_RIGHT], 
                _neighbors[EightWaySide.RIGHT]});
        } else if (rotationSuffix == "◥") {
            returned.AddRange(new List<T>() {
                _neighbors[EightWaySide.RIGHT], 
                _neighbors[EightWaySide.TOP_RIGHT], 
                _neighbors[EightWaySide.TOP]});
        } else if (rotationSuffix == "◤") {
            returned.AddRange(new List<T>() {
                _neighbors[EightWaySide.TOP], 
                _neighbors[EightWaySide.TOP_LEFT], 
                _neighbors[EightWaySide.LEFT]});
        }
        return returned.Where(x => x != null).ToList();
    }

    public T GetRandomNeighbor() {
        return _neighbors.Values.ToList()[UnityEngine.Random.Range(0,_neighbors.Values.Count)];
    }

}