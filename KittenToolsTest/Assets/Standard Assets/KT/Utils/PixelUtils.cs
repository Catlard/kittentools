﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PixelUtils {

	public static Sprite DrawFromBoolArray(bool[] drawing, int width, int height, Color c, Texture2D bgTexture, int drawingPadding) {

		Texture2D tex = new Texture2D(width, height);
		tex.wrapMode = TextureWrapMode.Clamp;
		tex.filterMode = FilterMode.Point;
		Color[] colors = new Color[width * height];
		Color[] bgColors = bgTexture.GetPixels();
		
		int drawingWidth = width-(drawingPadding * 2);

		//DREW NDX: 55. row 1 col 27 drawingNdx 26
		for(int i = 0; i < colors.Length; i++) {
			int row = Mathf.FloorToInt(i/width);
			int col = i % width;
			int drawingRow = row-1;
			int drawingCol = col-1;
			colors[i] = bgColors[i];
			int drawingNdx = (drawingRow * drawingWidth) + drawingCol;
			if(drawingRow > -1 && 
			   drawingCol > -1 && 
			   drawingCol < drawingWidth &&
			   drawingNdx < drawing.Length &&
			   drawing[drawingNdx]) {

				//    Debug.Log(string.Format("DREW NDX: {0}. row {1} col {2} drawingNdx {3}", i, row, col, drawingNdx));
				   colors[i] = c;
			}

		}
		tex.SetPixels(0,0,width, height, colors, 0);
		tex.Apply();
		return Sprite.Create(tex, new Rect(0,0, width, height), Vector2.one * .5f);
	}

	public static Texture2D Tex2DFromSprite(Sprite s) {
		if(s == null) {
			Debug.LogError("Sent nil sprite to Tex2DFromSprite");
			return null;
		}
		Rect r = s.rect;
		int x = (int) r.x; int y = (int) r.y; int w = (int) r.width; int h = (int) r.height;
		Texture2D tex = new Texture2D(w, h);
		tex.filterMode = FilterMode.Point;
		tex.name = s.name;
		tex.wrapMode = TextureWrapMode.Clamp;
		Color[] colors = s.texture.GetPixels(x,y,w,h, 0);
		tex.SetPixels(colors);
		tex.Apply();
		return tex;
	}


	public enum ImageFilterMode : int {
		Nearest = 0,
		Biliner = 1,
		Average = 2
	}
	public static Texture2D ResizeTexture(Texture2D pSource, ImageFilterMode pFilterMode, float pScale){
	
		//*** Variables
		int i;
	
		//*** Get All the source pixels
		Color[] aSourceColor = pSource.GetPixels();
		Vector2 vSourceSize = new Vector2(pSource.width, pSource.height);
	
		//*** Calculate New Size
		float xWidth = Mathf.RoundToInt((float)pSource.width * pScale);                     
		float xHeight = Mathf.RoundToInt((float)pSource.height * pScale);
	
		//******exit early?
		if(xWidth <= 0 || xHeight <= 0) return new Texture2D(0,0);

		//*** Make New
		Texture2D oNewTex = new Texture2D((int)xWidth, (int)xHeight, TextureFormat.RGBA32, false);
	
		//*** Make destination array
		int xLength = (int)xWidth * (int)xHeight;
		Color[] aColor = new Color[xLength];
	
		Vector2 vPixelSize = new Vector2(vSourceSize.x / xWidth, vSourceSize.y / xHeight);
	


		//*** Loop through destination pixels and process
		Vector2 vCenter = new Vector2();
		for(i=0; i < xLength; i++){
	
			//*** Figure out x&y
			float xX = (float)i % xWidth;
			float xY = Mathf.Floor((float)i / xWidth);
	
			//*** Calculate Center
			vCenter.x = (xX / xWidth) * vSourceSize.x;
			vCenter.y = (xY / xHeight) * vSourceSize.y;

			//*** Do Based on mode
			//*** Nearest neighbour (testing)
			if(pFilterMode == ImageFilterMode.Nearest){
	
				//*** Nearest neighbour (testing)
				if(pScale >= 2f) {
					vCenter.x = Mathf.Floor(vCenter.x);
					vCenter.y = Mathf.Floor(vCenter.y);
				} else {
					vCenter.x = Mathf.Round(vCenter.x);
					vCenter.y = Mathf.Round(vCenter.y);
				}
	
				//*** Calculate source index
				int xSourceIndex = (int)((vCenter.y * vSourceSize.x) + vCenter.x);
				
				//*** Copy Pixel
				if(xSourceIndex < aSourceColor.Length)
	 				aColor[i] = aSourceColor[xSourceIndex];
			}
	
			//*** Bilinear
			else if(pFilterMode == ImageFilterMode.Biliner){
	
				//*** Get Ratios
				float xRatioX = vCenter.x - Mathf.Floor(vCenter.x);
				float xRatioY = vCenter.y - Mathf.Floor(vCenter.y);
	
				//*** Get Pixel index's
				int xIndexTL = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
				int xIndexTR = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
				int xIndexBL = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
				int xIndexBR = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
	
				//*** Calculate Color
				aColor[i] = Color.Lerp(
					Color.Lerp(aSourceColor[xIndexTL], aSourceColor[xIndexTR], xRatioX),
					Color.Lerp(aSourceColor[xIndexBL], aSourceColor[xIndexBR], xRatioX),
					xRatioY
				);
			}
	
			//*** Average
			else if(pFilterMode == ImageFilterMode.Average){
	
				//*** Calculate grid around point
				int xXFrom = (int)Mathf.Max(Mathf.Floor(vCenter.x - (vPixelSize.x * 0.5f)), 0);
				int xXTo = (int)Mathf.Min(Mathf.Ceil(vCenter.x + (vPixelSize.x * 0.5f)), vSourceSize.x);
				int xYFrom = (int)Mathf.Max(Mathf.Floor(vCenter.y - (vPixelSize.y * 0.5f)), 0);
				int xYTo = (int)Mathf.Min(Mathf.Ceil(vCenter.y + (vPixelSize.y * 0.5f)), vSourceSize.y);
	
				//*** Loop and accumulate
				Vector4 oColorTotal = new Vector4();
				Color oColorTemp = new Color();
				float xGridCount = 0;
				for(int iy = xYFrom; iy < xYTo; iy++){
					for(int ix = xXFrom; ix < xXTo; ix++){
	
						//*** Get Color
						oColorTemp += aSourceColor[(int)(((float)iy * vSourceSize.x) + ix)];
	
						//*** Sum
						xGridCount++;
					}
				}
	
				//*** Average Color
				aColor[i] = oColorTemp / (float)xGridCount;
			}
		}
	
		//*** Set Pixels
		oNewTex.SetPixels(aColor);
		oNewTex.Apply();
		oNewTex.wrapMode = TextureWrapMode.Clamp;

		if(pFilterMode == ImageFilterMode.Nearest)
			oNewTex.filterMode = FilterMode.Point;

		//*** Return
		return oNewTex;
	}
}
