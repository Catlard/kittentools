﻿using UnityEngine;
using System.Collections;

public static class MathUtils {

	public static float RoundTo(float num, float rounding) {
		return Mathf.Round (num / rounding) * rounding;
	}

	public static Vector2 RoundTo(Vector2 v, float rounding) {
		return new Vector2(
			RoundTo(v.x, rounding),
			RoundTo(v.y, rounding)
		);
	}
	public static Vector3 RoundTo(Vector3 v, float rounding) {
		return new Vector3(
			RoundTo(v.x, rounding),
			RoundTo(v.y, rounding),
			RoundTo(v.z, rounding)
		);
	}

	public static int RandDirection() {
		return Random.value > .5f ? -1 : 1;
	}
}
