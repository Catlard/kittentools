﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR
	using UnityEditor;
#endif
		

public static class ResourcesUtils {

	public enum ResourceType {JSON, TXT};

	private static string GetExt(ResourceType t) {
		return "." + t.ToString().ToLower();
	}

	public static void SaveImageToDesktop(Texture2D tex) {
		string desktopPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "/";
		byte[] bytes = tex.EncodeToPNG();
		string filename = tex.name + ".png";
		Debug.Log(filename + " saved to " + desktopPath);
		System.IO.File.WriteAllBytes(desktopPath + "/" + filename, bytes);
	}

	//save a file to resources.
	 public static void Save(string fileName, ResourceType t, string contents) {
		fileName += GetExt(t);
		string path = Application.dataPath + "/Resources/" + fileName;
		Debug.Log("Resources saving to: " + path);
		using (FileStream fs = new FileStream(path, FileMode.Create))
		{
			using (StreamWriter writer = new StreamWriter(fs))
			{
				writer.Write(contents);
				writer.Close();
				writer.Dispose();
				#if UNITY_EDITOR
					AssetDatabase.ImportAsset("Assets/Resources/" + fileName); //Re-import the file to update the reference in the editor
				#endif
			}
			fs.Close();
			fs.Dispose();
		}
     }

	//load a file from resources.
	public static TextAsset Load(string fileName) {
		return Resources.Load<TextAsset>(fileName);
	}

	public static void Save(TLanguage fileName, ResourceType t, string contents) {Save("UNIQUE_CHARS_" + fileName.ToString(), t, contents); }
	public static void Save(DBKey fileName, ResourceType t, string contents) {Save(fileName.ToString(), t, contents); }
	public static TextAsset Load(DBKey fileName) { return Load(fileName.ToString()); }


	public static void Unload(Object obj) {
		Resources.UnloadAsset(obj);
	}

}
