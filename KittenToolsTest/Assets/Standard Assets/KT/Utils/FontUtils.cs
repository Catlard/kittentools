﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public static class FontUtils {

	//bitmap font maker uses binary expressed as powers of 2 to encode character positions. we decode them into bools here.
	public static bool[] GetBMFM2Bools(int character, TextAsset bitmapFontMakerText, int rowSize = 16) {
		Dictionary<string, object> chars = JsonConvert.DeserializeObject<Dictionary<string, object>>(bitmapFontMakerText.text);
		uint[] c = JsonConvert.DeserializeObject<uint[]>(chars[character.ToString()].ToString());
		List<string> bins = new List<string>();
		
		int sqrWidth = 0;
		for(int i = 0; i < c.Length; i++) {
			string bin = System.Convert.ToString (c[i], 2);
			if(bin.Length > sqrWidth) sqrWidth = bin.Length;
			if(bin == "0") continue;
			else bins.Add(bin);
		}

		if(bins.Count > sqrWidth)
			sqrWidth = bins.Count;

		for(int i = 0; i < bins.Count; i++) {
			while(bins[i].Length < sqrWidth)
				bins[i] = "0" + bins[i];
		}

		bool[] result = new bool[sqrWidth * sqrWidth];
		int ndx = 0;
		for(int i = bins.Count-1; i >=0; i--) {
			string bin = StringUtils.Reverse(bins[i]);
			for(int j = 0; j < bin.Length; j++) {
				result[ndx] = bin[j] == '1';
				ndx++;
			}
		}

		return result;
	}


}
