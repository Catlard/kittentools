﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public static class TransformUtils {

    public static GameObject MakeSquareSprite(bool show, bool addCollider = false) {
        GameObject tl = new GameObject("tl");
		Texture2D texture =  new Texture2D(100,100);
		Sprite sprite = Sprite.Create(texture, new Rect(0,0,100,100), Vector2.one * .5f);
		SpriteRenderer sr = tl.AddComponent<SpriteRenderer>();
		sr.sprite = sprite;
        Color c = ColorUtils.Random() * 1.5f;
		c.a = .5f;
        sr.color = c;
        if(!show)
            sr.enabled = false;

        return tl;
    }

    public static void ZeroLocalZ(Transform t) {
        Vector3 l = t.localPosition;
        l.z = 0;
        t.localPosition = l;
    }

    public static float RotationalDistInRadians(Quaternion a, Quaternion b) {
        float theta = Mathf.Acos(
            (a.w*b.w) + (a.x*b.x) + (a.y*b.y) + (a.z*b.z)
        );
        if (theta>Mathf.PI/2f) theta = Mathf.PI - theta;
        return theta;
    }

    public static T FindInParent<T>(Transform child) {
        T comp = child.GetComponent<T>();
        if(comp != null) {
            return comp;
        } else if(child.parent == null) {
            Debug.LogError("Tried to find a " + typeof(T).ToString() + " in the parents of " + StringUtils.GetHierarchyString(child));
            return default(T);
        } else {
            return FindInParent<T>(child.parent);
        }
    }

    public static Vector2 GetSnapScrollRectPosition(RectTransform target, RectTransform scrollRect, RectTransform contentPanel){
        Canvas.ForceUpdateCanvases();
        return (Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position) - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
    }

    public static TransformData CopyData(Transform t) {
        return new TransformData(t);
    }

    public static void PasteData(Transform giver, Transform receiver){
        PasteData(receiver, CopyData(giver));
    }

    public static void PasteData(Transform t, TransformData d) {
        t.position = d._position;
        t.rotation = d._rotation;
        t.localScale = d._scale;
    }

    
    
    public static Vector2 GetSizeDelta(Vector2 desiredSize, RectTransform rt){
        Vector2 oldSize = rt.sizeDelta;
        rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, desiredSize.x);
        rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, desiredSize.y);
        Vector2 desiredSizeDelta = rt.sizeDelta;
        rt.sizeDelta = oldSize;
        return desiredSizeDelta;
    }

    public static void RecursivelyEnableRenderers(Transform parent, bool enable) {
       Renderer[] rs = parent.GetComponentsInChildren<Renderer>();
       foreach(Renderer r in rs) {
           r.enabled = enable;
       }

    }

    public static Vector2 GetSizeDelta(RectTransform toCopy, RectTransform rt){
        return GetSizeDelta(new Vector2(toCopy.rect.width, toCopy.rect.height), rt);
    }

    public static void SetLayerRecursively(Transform p, string layerName, bool includeInactive = true){
       int layerInt = LayerMask.NameToLayer(layerName);
       SetLayerRecursively(p, layerInt, includeInactive);

    }

    public static void SetLayerRecursively(Transform p, int layerNumber, bool includeInactive = true)
    {
        foreach (Transform trans in p.GetComponentsInChildren<Transform>(includeInactive))
            trans.gameObject.layer = layerNumber;
    }


    public static Vector3[] GetCubePoints(Vector3 center, Vector3 extents, Quaternion rotation)
    {
        Vector3[] points = new Vector3[8];
        points[0] = rotation * Vector3.Scale(extents, new Vector3(1, 1, 1)) + center;
        points[1] = rotation * Vector3.Scale(extents, new Vector3(1, 1, -1)) + center;
        points[2] = rotation * Vector3.Scale(extents, new Vector3(1, -1, 1)) + center;
        points[3] = rotation * Vector3.Scale(extents, new Vector3(1, -1, -1)) + center;
        points[4] = rotation * Vector3.Scale(extents, new Vector3(-1, 1, 1)) + center;
        points[5] = rotation * Vector3.Scale(extents, new Vector3(-1, 1, -1)) + center;
        points[6] = rotation * Vector3.Scale(extents, new Vector3(-1, -1, 1)) + center;
        points[7] = rotation * Vector3.Scale(extents, new Vector3(-1, -1, -1)) + center;

        return points;
    }

    public static void DrawCubePoints(Vector3[] points, Color c, float duration)
    {
        Debug.DrawLine(points[0], points[1], c, duration);
        Debug.DrawLine(points[0], points[2], c, duration);
        Debug.DrawLine(points[0], points[4], c, duration);
        Debug.DrawLine(points[7], points[6], c, duration);
        Debug.DrawLine(points[7], points[5], c, duration);
        Debug.DrawLine(points[7], points[3], c, duration);
        Debug.DrawLine(points[1], points[3], c, duration);
        Debug.DrawLine(points[1], points[5], c, duration);
        Debug.DrawLine(points[2], points[3], c, duration);
        Debug.DrawLine(points[2], points[6], c, duration);
        Debug.DrawLine(points[4], points[5], c, duration);
        Debug.DrawLine(points[4], points[6], c, duration);
    }

    public static Bounds Get3DBounds(Transform objectWithMeshRenderers, bool includeInactive = true) {
        
        MeshRenderer[] allRenderers = objectWithMeshRenderers.gameObject.GetComponentsInChildren<MeshRenderer>(includeInactive);
        MeshRenderer m = objectWithMeshRenderers.GetComponent<MeshRenderer>();
        Bounds b = new Bounds(objectWithMeshRenderers.transform.position, Vector3.zero);
        if(m != null) 
            b.Encapsulate(m.bounds);
        foreach(MeshRenderer childM in allRenderers)
            b.Encapsulate(childM.bounds);

        // Debug.Log(objectWithMeshRenderers.name + " had " + allRenderers.Length);

        return b;
    }

    public static Rect RectTransformToScreenSpace(RectTransform transform)
     {
         Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
         Rect rect = new Rect(transform.position.x, Screen.height - transform.position.y, size.x, size.y);
         rect.x -= (transform.pivot.x * size.x);
         rect.y -= ((1.0f - transform.pivot.y) * size.y);
         return rect;
     }


    


}



[System.Serializable]
public class TransformData {
    public Vector3 _position;
    public Vector3 _scale;
    public Quaternion _rotation;
	public string _name = "n/a";

    public TransformData (Vector3 pos, Quaternion rot, Vector3 scale, string name) {
        _position = pos;
        _rotation = rot;
        _scale = scale;
		_name = name;
    }

    public TransformData () {}

    public override string ToString() {
        return string.Format("{3} R = {0}, P = {1}, S = {2}", _rotation, _position, _scale, _name);
    }


    public TransformData (Transform original) {
		_name = StringUtils.GetHierarchyString(original);
        _position = original.position;
        _rotation = original.rotation;
        _scale = original.localScale;
    }
}
