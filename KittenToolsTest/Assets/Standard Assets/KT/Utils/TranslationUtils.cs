﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class TranslationUtils {

	public static string SubstituteForWeirdCharacters(string maybeWeird) {
		if(maybeWeird == null)
			return "";
		maybeWeird = maybeWeird.Replace('’', '\'');
		maybeWeird = maybeWeird.Replace("…", "...");

		return maybeWeird;
	}

}
