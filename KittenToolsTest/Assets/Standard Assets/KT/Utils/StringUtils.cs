﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public static class StringUtils  {


	private static char[] vowels = { 'a', 'e', 'i', 'o', 'u' };
	private static string[] vowelDigraphs = { "au", "ee", "ai", "oo", "ui" };
	private static char[] consonants = { 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z' };
	private static string[] startPairs = { "th", "ch", "sh", "st", "sc", "sk", "sl", "pl", "tr", "pr", "cl", "fr" };
	private static string[] endPairs = { "th", "ch", "sh", "rn", "rg", "rd", "rk", "rt", "rp", "lp", "lm", "lk" };
	private enum ConsonantSoundType { Start, Middle, End }
	private static System.Random random = new System.Random((int)DateTime.Now.Ticks);//thanks to McAden
	private static string alpha = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890.,/+-_;[]()!@#$%^&*|`~";
	private static uint _tagCount = 0;

	//turns "27" into " 27", or 1 into "  1".
	public static string ZFillNumber(float number, string fillChar, int maxLength) {
		string numStr = number.ToString ();
		if (numStr.Length >= maxLength)
			return numStr;
		else {
			int numToAdd = maxLength - numStr.Length;
			for (int i = 0; i < numToAdd; i++) {
				numStr = fillChar + numStr;
			}
			return numStr;
		}
	}

	public static string Reverse(string s) {
        char[] arr = s.ToCharArray();
        Array.Reverse(arr);
        return new string(arr);
    }

	public static string MakePercent(float amt, float max) {
		return Mathf.Round(amt / max * 100f) + "%";
	}

	public static string ReplaceFirst(string text, string search, string replace)
	{
	  int pos = text.IndexOf(search, StringComparison.CurrentCulture);
	  if (pos < 0)
	  {
	    return text;
	  }
	  return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
	}

	public static int CountApproximateSyllables(string word) {
		word = word.ToLower().Trim();
		bool lastWasVowel = false;
		var vowels = new []{'a','e','i','o','u','y'};
		int count = 0;

		//a string is an IEnumerable<char>; convenient.
		foreach(var c in word)
		{
			if(vowels.Contains(c)) {
				if(!lastWasVowel)
					count++;
				lastWasVowel = true;
			} else {
				lastWasVowel = false;
			}                     
		}

		if ((word.EndsWith("e") || word.EndsWith("es") || word.EndsWith("ed")) && !word.EndsWith("le"))
			count--;
		
		return count;
	}

	public static string DumpRectangularArray(object[,] arr) {
		string msg = "";
		for(int x = 0; x < arr.GetLength(0); x++) {
			for(int y = 0; y< arr.GetLength(1); y++) {
				if(arr[x,y] == null)
					msg += "N";
				else if (arr[x,y].ToString() == "")
					msg += '-';
				else
					msg += arr[x,y].ToString();
			}
			msg += "\n";
		}
		return msg;
	}


	public static string MakeTag(string prefix) {
		_tagCount++;
		if(_tagCount > 4294967000)
			_tagCount = 0;
		return prefix + _tagCount.ToString();
	}

	public static string GetHierarchyString(Transform child) {
		string hierarchy = child.name;
		Transform inspected = child;
		while(inspected.parent != null) {
			hierarchy = inspected.parent.name + "/" + hierarchy;
			inspected = inspected.parent;
		}
		return hierarchy;

	}

	public static int Hashify(string key) {
		string hash = "";
		char[] chars = key.ToCharArray();
		
		for(int i = 0; i< chars.Length; i++ ) {
			if(alpha.Contains(chars[i].ToString())) {
				string ndx = alpha.IndexOf(chars[i]).ToString();
				// DBug.print(chars[i], ndx);
				hash += ndx;
			}
		}

		//longest ulong is 184 467 440 737 095 516 15, so need to be <= 19 chars
		int limit = 19;
		int over = hash.Length - limit;
		if(over > 0) {
			// Debug.LogWarning("You are hashing something too long. " + key + " produces a number longer than 19 characters. Wrapped your hash around.");
			int startNdx = key.Length % over;
			hash = hash.Substring(startNdx, limit);
		}

		ulong hashed = ulong.Parse(hash);
		hashed = hashed % 2147483647; //put in int range
		return (int) hashed;
	}



	public static string FirstLetterToUpper(string str)
	{
		if (str == null)
			return null;

		if (str.Length > 1)
			return char.ToUpper(str[0]) + str.Substring(1);

		return str.ToUpper();
	}





	private static string GetVowelSound() {
		string res = "";
		int i = random.Next(vowels.Length * 3 + vowelDigraphs.Length);
		if (i >= vowels.Length) i -= vowels.Length;
		if (i >= vowels.Length) i -= vowels.Length;
		if (i >= vowels.Length) {
			res += vowelDigraphs[i - vowels.Length];
		} else {
			res += vowels[i];
		}
		return res;
	}


	private static string GetConsonantSound(ConsonantSoundType type) {
		string res = "";
		if (type == ConsonantSoundType.Start) {
			int n = consonants.Length + startPairs.Length;
			int i = random.Next(n);
			if (i < consonants.Length) {
				res += consonants[i];
			} else {
				res += startPairs[i - consonants.Length];
			}
		} else if (type == ConsonantSoundType.End) {
			int n = consonants.Length + endPairs.Length;
			int i = random.Next(n);
			if (i < consonants.Length) {
				res += consonants[i];
			} else {
				res += endPairs[i - consonants.Length];
			}
		} else {
			int i = random.Next(consonants.Length);
			res += consonants[i];
		}
		return res;
	}

	public static string RandomName() {
		string res = "";
		if (random.Next(6) != 0) {
			res += GetConsonantSound(ConsonantSoundType.Start);
		}
		res += GetVowelSound();
		res += GetConsonantSound(ConsonantSoundType.Middle);
		res += GetVowelSound();
		if (random.Next(4) != 0) {
			res += GetConsonantSound(ConsonantSoundType.End);
		}
		char[] ca = res.ToCharArray();
		ca[0] = char.ToUpper(res[0]);
		return new string(ca);
	}

	public static string GetColoredTMProText(Color color, string wrapped) {
		string hex = "#" + ColorUtility.ToHtmlStringRGB(color);
		return GetColoredTMProText(hex, wrapped);

	}

	public static string GetColoredTMProText(string hexColor, string wrapped) {
		return "<color=" + hexColor + ">" + wrapped + "</color>";
	}

	public static string RainbowTMProText(string[] hexColors, string toBeColored) {
		string final = "";
		char[] chars = toBeColored.ToCharArray();
		for(int i = 0; i < chars.Length; i++) {
			
			int ndx = i > 0 ? (i % hexColors.Length) : 0;
			final += GetColoredTMProText(hexColors[ndx], chars[i].ToString());
		}
		return final;
	}

}
