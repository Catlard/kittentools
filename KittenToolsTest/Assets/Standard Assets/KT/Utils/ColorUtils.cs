﻿using UnityEngine;
using System.Collections;

public static class ColorUtils {

	public static readonly Color clearWhite = new Color(1,1,1,0);

	public static Color Clarify(Color c) {
		c.a = 0;
		return c;
	}

	public static Color SetAlpha(Color c, float newAlpha) {
		c.a = newAlpha;
		return c;
	}

	public static void SetAlpha(SpriteRenderer c, float newAlpha) {
		c.color = SetAlpha(c.color, newAlpha);
	}

	public static Color Mutate(Color c, float aberration, bool alpha = false) {
		float min  = -aberration/2;
		
		return new Color(
			c.r + min + (UnityEngine.Random.value * aberration),
			c.g + min + (UnityEngine.Random.value * aberration),
			c.b + min + (UnityEngine.Random.value * aberration),
			alpha ? c.a + min + (UnityEngine.Random.value * aberration) : c.a
		);
	}

	public static Color Random(float lowest = 0, float highest = 1) {
		return new Color(
			UnityEngine.Random.Range(lowest, highest), 
			UnityEngine.Random.Range(lowest, highest), 
			UnityEngine.Random.Range(lowest, highest), 
			1
		);
	}

	public static Color HexToRGB(string hex) {
		if(hex.Substring(0,1) != "#") {
			hex = "#" + hex;
		}

		Color finished = Color.cyan;
		if(ColorUtility.TryParseHtmlString(hex, out finished)) {
			return finished;
		} else {
			Debug.LogWarning("HexToRGB: Coudln't parse " + hex);
			return Color.cyan;
		}
	}

	public static Color ColorFromString(string s) {
		if (s == "red") {
			return Color.red;
		}
		else if (s == "green") {
			return Color.green;
		}
		else if (s == "yellow") {
			return Color.yellow;
		}
		else if (s == "blue") {
			return Color.blue;
		}          
		return Color.white;
	}

	public static Color RoundColor(Color color, float rounding) {
		color.r = MathUtils.RoundTo(color.r, .01f);
		color.g = MathUtils.RoundTo(color.g, .01f);
		color.b = MathUtils.RoundTo(color.b, .01f);
		color.a = MathUtils.RoundTo(color.a, .01f);
		return color;
	}



}
