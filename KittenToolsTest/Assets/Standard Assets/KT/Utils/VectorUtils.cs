﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class VectorUtils {

	public static int ManhattanDistance(Vector2Int a, Vector2Int b) {
		return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
	}

	public static float ManhattanDistance(Vector2 a, Vector2 b) {
		return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
	}

    public static bool PolygonContainsPoint(Vector2[] polyPoints, Vector2 p)
    {
        var j = polyPoints.Length - 1;
        var inside = false;
        for (int i = 0; i < polyPoints.Length; j = i++)
        {
            var pi = polyPoints[i];
            var pj = polyPoints[j];
            if (((pi.y <= p.y && p.y < pj.y) || (pj.y <= p.y && p.y < pi.y)) &&
                (p.x < (pj.x - pi.x) * (p.y - pi.y) / (pj.y - pi.y) + pi.x))
                inside = !inside;
        }
        return inside;
    }



	public static Vector3 GetRandomDirections(Vector3 input) {
		if (Random.value < .5f)
			input.y *= -1f;
		if (Random.value < .5f)
			input.x *= -1f;
		if (Random.value < .5f)
			input.z *= -1f;

		return input;
	}

	public static Vector3 GetRandomVector() {
		return new Vector3(
			Random.value, Random.value, Random.value
		);
	}

	public static Vector2 Invert(Vector2 v) {
		return new Vector2(v.y, v.y);
	}

	public static Vector2 Rotate(this Vector2 v, float degrees) {
         float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
         float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
         
         float tx = v.x;
         float ty = v.y;
         v.x = (cos * tx) - (sin * ty);
         v.y = (sin * tx) + (cos * ty);
         return v;
     }

	//Properly Lerp between two angles
    public static Vector3 AngleLerp(Vector3 StartAngle, Vector3 FinishAngle, float t)
     {        
         float xLerp = Mathf.LerpAngle(StartAngle.x, FinishAngle.x, t);
         float yLerp = Mathf.LerpAngle(StartAngle.y, FinishAngle.y, t);
         float zLerp = Mathf.LerpAngle(StartAngle.z, FinishAngle.z, t);
         Vector3 Lerped = new Vector3(xLerp, yLerp, zLerp);
         return Lerped;
     }

	public static float Get360Angle(Vector2 a, Vector2 b) {
		return ((Mathf.Atan2 (a.y - b.y, a.x - b.x) * Mathf.Rad2Deg) + 360) % 360;  // +360 for implementations where mod returns negative numbers
	}

	public static Vector3 GetRandomDirections() {
		return GetRandomDirections(Vector3.one);
	}
		
	public static Vector3 GetRandomDirections(float x, float y, float z) {
		return GetRandomDirections (new Vector3 (x, y, z));
	}


	//when changing a localposition, divide by the results of this function to make your movement correct.
	public static Vector3 GetBoundsScale(Transform child) {
		Vector3 totalScale = Vector3.one;
		Transform t = child;
		while (t.parent != null) {
			totalScale = Vector3.Scale(t.parent.localScale, totalScale);
			t = t.parent;
		}
		return totalScale;
	}

	public static Vector2 GetUnitCirclePosition(float degrees) {
		float rads = Mathf.Deg2Rad * degrees;
		float x = Mathf.Pow (Mathf.Sin (rads), 1);
		float y = Mathf.Pow (Mathf.Cos (rads), 1);

		return new Vector2 (x, y);	
	}

	public static bool ContainsPoint (Vector2[] polyPoints, Vector2 p) { 
		var j = polyPoints.Length-1; 
		var inside = false; 
		for (int i = 0; i < polyPoints.Length; j = i++) { 
			if ( ((polyPoints[i].y <= p.y && p.y < polyPoints[j].y) || (polyPoints[j].y <= p.y && p.y < polyPoints[i].y)) && 
				(p.x < (polyPoints[j].x - polyPoints[i].x) * (p.y - polyPoints[i].y) / (polyPoints[j].y - polyPoints[i].y) + polyPoints[i].x)) 
				inside = !inside; 
		} 
		return inside; 
	}

	public static Vector3 InverseLerp(Vector3 a, Vector3 b, Vector3 current) {
		Vector3 totalDiff = b - a;
		Vector3 currDiff = current - a;
		return new Vector3 (currDiff.x / totalDiff.x, currDiff.y / totalDiff.y, currDiff.z / totalDiff.z);
	}

	public static float InverseLerpToFloat(Vector3 a, Vector3 b, Vector3 value)
     {
         Vector3 AB = b - a;
         Vector3 AV = value - a;
         return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
     }

	/// <summary>
	/// Only works when it is for sure on the shortest line between the two vectors. If it is off the line, this won't work.
	/// </summary>
	/// <returns>The lerp2 d.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	/// <param name="current">Current.</param>
	public static float InverseLerp2D(Vector3 a, Vector3 b, Vector3 current) {
		Vector3 v =  InverseLerp (a, b, current);
		DBug.print (v);
		return (v.x + v.y) / 2f;
	}

	//returning (.5,.5) means it's in the center of the screen.
	public static Vector2 WorldObjPosToScreenPercent(Vector3 objPos, Bounds orthoBounds) {
		return new Vector2 ((objPos.x - orthoBounds.min.x) / (orthoBounds.max.x - orthoBounds.min.x),
			(objPos.y - orthoBounds.min.y) / (orthoBounds.max.y - orthoBounds.min.y));
	}

	public static Vector2 ScreenPercentToWorldObjPos(Bounds camBounds, Vector2 percent) {
		return new Vector2(
			Mathf.Lerp(camBounds.min.x, camBounds.max.x, percent.x),
			Mathf.Lerp(camBounds.min.y, camBounds.max.y, percent.y)
		);

	}

	public static Vector3 Scale(Vector3 a, Vector3 b) {
		return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
	}

	public static Vector3 ClampValues(Vector3 input, float min, float max) {
		input.x = Mathf.Clamp(input.x, min, max);
		input.y = Mathf.Clamp(input.y, min, max);
		input.z = Mathf.Clamp(input.z, min, max);
		return input;
	}

	

	//adapted from here:
	//https://forum.unity.com/threads/how-to-calculate-force-needed-to-jump-towards-target-point.372288/

	public static Vector2? GetJumpForceRequiredToHitPoint(Rigidbody2D body, Vector3 currentPos, Vector3 targetPos, float velocity, bool hitStraight) {
 
		float v = velocity;
		float x = targetPos.x - currentPos.x;
		float y = targetPos.y - currentPos.y;
		float g = body.gravityScale * -Physics2D.gravity.y;

		float dis = Mathf.Pow(v, 4) - (2 * v * v * g * y) - (g * g * x * x);

		//If the discriminant is less than 0, that means the projectile can't reach the
		//target as the square root of a negative number is imaginary.
		//So we need to account for that.
		if (dis > 0) {
			float plusminus = Mathf.Sqrt(dis);
			plusminus *= hitStraight ? -1 : 1;
			float dividend = v*v + plusminus;

			//For once we actually don't want atan2 - it'd mess with our results.
			float theta = Mathf.Atan(dividend/(g*x));
			//Instead we just flip the vector if the target is on the left
			int dir = x > 0 ? 1 : -1;
			return new Vector2(
				dir * v * Mathf.Cos(theta), 
				dir * v * Mathf.Sin(theta)
			);
		} else {
			//Whatever you want to do here if the target can't be reached.
			//You could do something cool here like an angle for an "attempted" shot.
			return null;
		}











	}

	//returns 2d path!

	//Review

	//If air resistance is ignored, the height h of a projectile above the ground after t seconds is given by 
	//H(t) = - (1 / 2) g t 2 + Vo t + Ho

	//where g is the acceleration due to gravity which on earth is approximately 
	//equal to 32 feet / s 2, Vo is the initial velocity (when t = 0 )
	// and Ho is the initial height (when t = 0).
	public static Vector3[] Get2DProjectilePath(
		Vector2 jumpForce, 
		Rigidbody2D _body, 
		Vector3 startPos,
		int density,
		float floorHeight
	) {

            float totalYV = jumpForce.y * -2;
            float grav = Physics2D.gravity.y * _body.gravityScale;

            float timeInAir =  totalYV / grav;

            float xDistTravelled = timeInAir * jumpForce.x;
			if(jumpForce.y < 0) {
				xDistTravelled *= -1;
			}

            Vector3 finalPos = startPos;
            finalPos.x += xDistTravelled;
            float gravMult = .5f * grav;

            List<Vector3> points = new List<Vector3>();
			bool continueCalculating = true;
			int i = 0;
			int tries = 300;

			// string msg = "";

			// changed direction, not just negative
            while(continueCalculating) {
                float percent = (float) i / density;
                float dTime = timeInAir * percent;
				int dir = jumpForce.y > 0 ? 1 : -1;
				float gravPower = (gravMult * Mathf.Pow(dTime,2));
				float jumpPower = jumpForce.y * dTime * dir;
                float height = gravPower + jumpPower + startPos.y;

				// if(i < 5) {
				// 	msg += i + ": " + height + " grav: " + gravPower + " jump: " + jumpPower + " start: " + startPos.y + "\n";
				// 	msg += "-----" + jumpForce.y + " // " + dTime + "\n";
				// 	msg += "======" + startPos.x + " -- " + finalPos.x + " -- " + percent + " \n";
				// }

                points.Add(new Vector3(
                    Mathf.LerpUnclamped(startPos.x, finalPos.x, percent),
                    height,
                    startPos.z
                ));

				i++;



				if(i > tries) {
					continueCalculating = false;
					// Debug.LogWarning("More than " +  tries  + " points in vector list. Sub optimal.");
				} else if(points.Count >= 2) {
					float yThis = points[points.Count-1].y;
					float yPrev= points[points.Count-2].y;
					if(yThis < yPrev) {
						if(yThis <= floorHeight) {
							continueCalculating = false;
						}
					}

				}
            }
			
			// Debug.Log(msg);

			return points.ToArray();
	}


}
