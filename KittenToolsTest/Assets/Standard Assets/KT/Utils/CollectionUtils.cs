﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class CollectionUtils {

	public static List<T> Shuffle<T>(List<T> list)
	{
		for (int i = list.Count; i > 1; i--)
		{
			// Pick random element to swap.
			int j = Mathf.FloorToInt(UnityEngine.Random.value * list.Count);
			// Swap.
			T tmp = list[j];
			list[j] = list[i - 1];
			list[i - 1] = tmp;
		}

		return list;
	}

	//
	public static List<T> GetCyclicPermutation<T>(List<T> list, int pops) {
		for(int i = 0; i < pops; i++) {
			list.Add(list[0]);
			list.RemoveAt(0);
		}
		return list;
	}

	public static T[] Shuffle<T>(T[] array)
	{
		for (int i = array.Length; i > 1; i--)
		{
			// Pick random element to swap.
			int j = Mathf.FloorToInt(UnityEngine.Random.value * array.Length); // 0 <= j <= i-1
			// Swap.
			T tmp = array[j];
			array[j] = array[i - 1];
			array[i - 1] = tmp;
		}
		return array;
	}

	public static T GetRandom<T>(T[] array) {
		return array[UnityEngine.Random.Range(0,array.Length)];
	}

	public static T GetRandom<T>(List<T> list) {
		return list[UnityEngine.Random.Range(0,list.Count)];
	}

	public static void Each<T>(this IEnumerable<T> ie, Action<T, int> action)
	{
		var i = 0;
		foreach (var e in ie) action(e, i++);
	}

	
	
}
