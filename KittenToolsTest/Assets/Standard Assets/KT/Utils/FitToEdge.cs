﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FitToEdge : AspectRatioFitter {

	protected override void OnRectTransformDimensionsChange ()
	{
		aspectRatio = (float)Screen.width / Screen.height;
		base.OnRectTransformDimensionsChange ();

	}
}
