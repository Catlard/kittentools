﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

public static class EnumUtils {
	public static T GetRandomEnum<T>()
	{
	   System.Array A = System.Enum.GetValues(typeof(T));
 	   T V = (T)A.GetValue(UnityEngine.Random.Range(0,A.Length));
 	   return V;
	}

    public static IEnumerable<T> GetValues<T>() {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }
	
	public static TEnum Parse<TEnum>(string value) where TEnum : struct
	{
   		return (TEnum)Enum.Parse(typeof(TEnum), value);
	}

	public static T GetNext<T>(T t) where T : Enum {
		T[] ts = EnumUtils.GetValues<T>().ToArray();
		int current = 0;
		for(int i = 0; i < ts.Length; i++) {
			if(t.Equals(ts[i])) {
				current = i;
			}
		}
		current++;
		if(current >= ts.Length) {
			current = 0;
		}
		return ts[current];

	}
	
	
}