using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC; //mec needs this.


namespace KTExample {

    public class KTExampleController : SingletonKTBehavior<KTExampleController>
    {

        string _pauseableUpdateTag;
        string _printSomethingTag; 

        // Start is called before the first frame update
        void Start()
        {
            Routines.Init(); //once per run of the app, needs to happen
            DB.Init(); //start this service.

            //initialize mvc
            KTExampleModel.instance.Init(); //check startup count
            KTExampleView.instance.Init(); //load sprites, sounds

            //create unique, registered tags for a coroutines, so they can be killed later
            _pauseableUpdateTag = Routines.GetTag(this, "update");
            _printSomethingTag = Routines.GetTag(this, "print");
            
            Timing.RunCoroutine(GameLoop(1,2,3), _pauseableUpdateTag);
            Timing.RunCoroutine(PrintSomethingAfterATime(), _printSomethingTag);
        }

        // Update is called once per frame.
        // runs as quickly as possible, forever. check the TimeUtils function DoUpdate for a shortcut.
        IEnumerator<float> GameLoop(int youCan, int sendMultipleParams, int unlikeInUnity) {
            while(true) {
                yield return Timing.WaitForOneFrame; 

                if(Input.GetKeyDown(KeyCode.Space)) { //get input!
                    KTExampleView.instance.MakeAndTweenASprite();
                } else if(Input.GetKeyDown(KeyCode.X)) {
                    Routines.Pause(_printSomethingTag); //pause other coroutine
                    KTExampleView.instance.PauseAnimation();
                } else if(Input.GetKeyDown(KeyCode.C)) {
                    Routines.Resume(_printSomethingTag); //resume
                    KTExampleView.instance.ResumeAnimation();
                } else if(Input.GetKeyDown(KeyCode.V)) {
                    KTExampleView.instance.PlayTheSound();
                }

            }
        }

        IEnumerator<float> PrintSomethingAfterATime() {
            yield return Timing.WaitForSeconds(2);
            DBug.print("waited", 4, "seconds, then printed!");
            KTExampleView.instance.PlayTheSound();
        }
    }

}
