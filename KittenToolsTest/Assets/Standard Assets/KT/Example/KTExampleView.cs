using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; //dotween needs this

namespace KTExample {

    public class KTExampleView : SingletonKTBehavior<KTExampleView>
    {

        SpriteHolder _holder;
        Deck<Sprite> _btSprites;
        string _animationTag;

        public void Init() {
        
            _holder = gameObject.AddComponent<SpriteHolder>(); //add this component
            _holder.AddFromResources("PixelKit"); //load sprites from resources folder

            Sprite[] bts = _holder.GetSpritesWithPattern("bt"); //get all sprites with "bt" in the name.
            print("Found " + bts.Length + " bt sprites!");

            //load them into a deck, so can be randomly gotten later.
            _btSprites = new Deck<Sprite>();
            _btSprites.Init(bts);

            //create a unique, registered tag for animating, so we can cancel ones that we've started
            _animationTag = Routines.GetTag(this, "tweener");

            //start up the sound library
            SoundLibrary.instance.Init();

        }

        public void MakeAndTweenASprite() {            
            Routines._routines.Dump(); //show what's happening in the console.
            Routines.Kill(_animationTag); //cancel all previous routines
            Sprite chosen = _btSprites.Get(); //get a random sprite from the deck

            //make a sprite on screen with a nice name
            string name = chosen.name + DB.GetPref<int>(DBKey.KT_EXAMPLE_STARTUPS, 0);
            GameObject go = new GameObject(name);
            var renderer = go.AddComponent<SpriteRenderer>();
            renderer.sprite = chosen;
            go.transform.position = GameObject.Find("Camera").transform.position + new Vector3(0,0,2); //put it in front of the camera
            Vector3 destination = Vector3.Scale(VectorUtils.GetRandomVector(), VectorUtils.GetRandomDirections()) * 2;
            destination.z = go.transform.position.z;

            go.transform.DOMove(destination, 3) //start a tween
                .SetEase(Ease.InOutBack) //set the easing
                .SetId(_animationTag); //set the animation to use a tag


        }

        public void PauseAnimation() {
            Routines.Pause(_animationTag);
        }

        public void ResumeAnimation() {
            Routines.Resume(_animationTag);
        }

        //use the sound library to play a sound.
        public void PlayTheSound() {
            SoundParams parms = new SoundParams(
                name: "SampleSound", 
                pitchVariance: .2f, 
                volume: .5f,
                loops: 1
            ); //create an object that can be used to play a sound.
            PlayingSound sound = SoundLibrary.instance.PlaySound(parms); //play the sound.
            //create an effect while the sound is playing.
            sound.TweenPitch(.5f, sound._source.clip.length);
        }
    }

}
