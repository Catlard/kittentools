using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KTExample {

    public class KTExampleModel : SingletonKTBehavior<KTExampleModel>
    {
        public void Init() {
            //remember how many times you have started up this example.
            int startups = DB.GetPref<int>(DBKey.KT_EXAMPLE_STARTUPS, 0); //get from memory
            startups++; //increment
            print("You've started up this example " + startups + " times!");
            DB.SetPref<int>(DBKey.KT_EXAMPLE_STARTUPS, startups); //save for next time
        }

    }

}
