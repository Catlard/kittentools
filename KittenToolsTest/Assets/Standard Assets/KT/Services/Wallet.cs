﻿/* A simple implementation for microtransaction coins. Saves to the DB when spending is successful.*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Wallet : SingletonKTBehavior<Wallet>{

	public bool _debug;
	public bool _forceCoinCount;
	public int _coinsInWallet;


	private void Log(object msg){
		if (!_debug) return;
		print("WALLET: " + msg.ToString() + "Coins = " + _coinsInWallet);
	}

	public void Init(List<object> parms){

		if (parms == null){
			parms = new List<object>();
			parms.Add(false); //download in fake startup?
			parms.Add(new SetupTask(null, null, "FakeUIDataLoad"));
		}

		_debug = (bool) parms[0];

		if (_forceCoinCount)
			DB.SetPref(DBKey.COINS_IN_WALLET, _coinsInWallet);
		_coinsInWallet = DB.GetPref<int>(DBKey.COINS_IN_WALLET, 0);
		
		
		Log("Init.");
		
		SetupTask thisTask = (SetupTask) parms[1];
		var onComplete = (Action<SetupTask>) parms[2];
		onComplete.Invoke(thisTask);
	}

	public void SaveCoins(){
		DB.SetPref(DBKey.COINS_IN_WALLET, _coinsInWallet);
	}

	public bool SpendCoins(int howMany){
		if (_coinsInWallet >= howMany){
			Log("Spending " + howMany + " coins.");
			_coinsInWallet -= howMany;
			SaveCoins();
			return true;
		}
		else{
			Log("Cannot spend " + howMany + ". Not enough coins.");
			return false;

		}
	}

	public void AddCoins(int howMany){
		_coinsInWallet += howMany;
		SaveCoins();
	}
}
