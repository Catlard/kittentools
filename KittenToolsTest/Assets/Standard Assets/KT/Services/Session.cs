/* Functions like DB, but does not persist. Survives app suspends, but not quits.*/

using UnityEngine;
using System.Collections.Generic;

public static class Session {

	private static bool _initted;
	private static bool _debug = false;
	public static string _sessionName;
	private static Dictionary<string, object> _sessionData;

	//log messages identifying the session.
	private static void Log(string message) {
		if (_debug)
			Debug.Log ("Session: " + _sessionName + ": " + message);
	}

	//initialize will cause the session to destroy itself if it's a duplicate. otherwise, installs itself as the session.
	public static void Init() {
		if (_initted) return;
		_sessionData = new Dictionary<string, object> ();
		_sessionName = StringUtils.RandomName () + "_" + UnityEngine.Random.Range (1, 1000000);
		_initted = true;
	}

	public static void Clear() {
		_initted = false;
		Init();
	}

	//store and load data to/from a dict.
	public static T GetPref<T>(string keyName, T def) {
		if (!_initted) Init ();

		T returned = def; //assign default?

		if (_sessionData.ContainsKey (keyName)) {
			returned = (T)_sessionData [keyName];
			Log ("Found key " + keyName + "=" + returned.ToString());
		} else {
			Log ("No value for key: " + keyName + ". Returning default.");
		}
		return returned;
	}

	public static void SetPref<T>(string keyName, T val) {
		if (!_initted) Init ();
		Log("Added " + keyName + " (value = " + val.ToString() + ")");
		if (_sessionData.ContainsKey (keyName))
			_sessionData [keyName] = val;
		else
			_sessionData.Add (keyName, val);
	}
}
