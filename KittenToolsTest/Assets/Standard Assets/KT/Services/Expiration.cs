﻿using UnityEngine;

/* I use this class for making builds that expire and break on their own, so
people can't just freely distribute APKs that I give them. Set the _daysAllowed variable
To whatever you want, and check the HasExpired() function to get whether you're past 
the expiry date. */


public static class Expiration {

	public static float _daysLeft;
	static readonly float _daysAllowed = 30;

	public static void Init(bool active, bool debug) {
		if(Application.isEditor) 
			DB.SetPref<long>(DBKey.LAST_BUILD_TIME, TimeUtils.GetUnixTime());
		if(!active) {
			_daysLeft = Mathf.Infinity;
			return;
		}

		long lastBuildTime = DB.GetPref<long>(DBKey.LAST_BUILD_TIME, TimeUtils.GetUnixTime());
		long now = TimeUtils.GetUnixTime();
		float secondsSinceBuild = now - lastBuildTime;
		float day = 60 * 60 * 24;
		_daysLeft = _daysAllowed - (secondsSinceBuild/day);

		if(active) {
			if(debug)
				Debug.Log("Build expires in: " + Mathf.FloorToInt(_daysLeft) + " days.");
		}
	}

	public static bool HasExpired() {
		return _daysLeft < 0;
	}
}
