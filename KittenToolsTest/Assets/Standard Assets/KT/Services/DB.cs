﻿/* 
A helper class which makes it much easier to work with the PlayerPrefs
Functionality provided by Unity. This data persists in between sessions,
and can handle any type. I prefer to use an enum called DBKey, to prevent
using strings all the time for lookups (this prevents spelling errors, and
allows me to rename things easily). So there are overloaded functions which
allow for DBKey to be used as well. I have included a sample DBKey declaration
for your usage, in DBKey.cs. Try it out!

I have also included "pickleArray
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Text.RegularExpressions;
using System.Linq;
using System;

public static class DB {

	public static string _dBTypeDelimiter = "!~"; //have to be regex safe
	public static string _pickleDelimiter = "~~"; //have to be regex safe
	public static bool _debug = false;


	public static Action Init() {
		return ClearAll;
	}

	public static void SetPref<T>(DBKey k, T value) {	
		SetPref<T>(k.ToString(), value);
	}

	public static void SetPref<T>(string keyName, T value) {
		if(value == null) {
			Debug.LogError("Could not set pref to null value: "
				+ keyName);
			return;
		}
		if(_debug) DBug.print ("DB SET: " + keyName + " to " + value.ToString ());
		string encryptedKey = Crypto.Encrypt (keyName);			
		string typeName = typeof(T).ToString();
		string toEncrypt = value.ToString () + _dBTypeDelimiter + typeName;
		string encryptedValue = Crypto.Encrypt (toEncrypt);
		PlayerPrefs.SetString (encryptedKey, encryptedValue);
	}
	
	public static int ResetKeysWithPrefixes(string[] prefixes) {
		DBKey[] keys = (DBKey[]) EnumUtils.GetValues<DBKey>();
		int cleared = 0;
		string msg = "";
		foreach(string prefix in prefixes) {
			msg +=  " " + prefix;
		}
		for(int i = 0; i < keys.Length; i++) {
			string v = keys[i].ToString();
			foreach(string prefix in prefixes) {
				if(v.Substring(0,prefix.Length) == prefix) {
					cleared++;
					Clear(keys[i]);
				}
			}
		}
		Debug.Log("DB: Erased " + cleared + " keys for prefixes:" + msg + ".");
		return cleared;
	}

	public static T GetPref<T>(DBKey k, T defaultValue) {
		return GetPref<T>(k.ToString(), defaultValue);
	}

	public static bool HasPref(DBKey key) {
		return HasPref(key.ToString());

	}

	public static bool HasPref(string key) {
		return PlayerPrefs.HasKey(Crypto.Encrypt(key));
	}

	public static T GetPref<T>(string keyName, T defaultValue) {
		string encryptedKey = Crypto.Encrypt (keyName);
		if (PlayerPrefs.HasKey (encryptedKey)) {

			string decryptedString = Crypto.Decrypt(PlayerPrefs.GetString(encryptedKey, defaultValue.ToString()));
			string[] splitUp = Regex.Split(decryptedString,_dBTypeDelimiter);
			if (splitUp.Length != 2) {
				Debug.LogError ("DB ERROR: Could not properly split string: " + decryptedString);
				return defaultValue;
			} else {

				T returned = TypeUtils.Parse<T> (splitUp [0]);
				if(_debug) DBug.print ("DB RETURNED: " + keyName + ", " + splitUp[0].ToString () + "(" + returned.ToString() + ")");
				return returned;
			}
		} else {
			if(_debug) DBug.print ("DB RETURNED DEFAULT: " + keyName + ", " + defaultValue.ToString ());

			return defaultValue;
		}
	}


	//a way to quickly get whether a pref has been set to true or not. autosets it to true if it didn't exist.
	public static bool ConsumePref(string keyName) {
		bool consumed = GetPref<bool> (keyName, false);
		if (!consumed) {
			SetPref<bool> (keyName, true);
			return true;
		} else
			return false;
	}

	public static bool ConsumePref(DBKey key) {
		return ConsumePref(key.ToString());
	}

	private static void ClearAll() {
		PlayerPrefs.DeleteAll ();
		if(_debug) Debug.Log("DB: Cleared all except " + DBKey.LAST_BUILD_TIME);
	}

	public static void Clear(string key) {
		string encryptedKey = Crypto.Encrypt (key);
		PlayerPrefs.DeleteKey (encryptedKey);
	}
	
	public static void Clear(DBKey key) {
		Clear(key.ToString());
	}

}
