﻿
//Encryption helper functions.

using System;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Compression;
using System.IO;
using KT;
using Newtonsoft.Json;

public static class Crypto
{
	private static byte[] key = new byte[8] {1, 2, 3, 4, 5, 6, 7, 8};
	private static byte[] iv = new byte[8] {1, 2, 3, 4, 5, 6, 7, 8};


	public static string Base64Encode(string plainText) {
		var plainTextBytes = System.Text.Encoding.ASCII.GetBytes(plainText);
		return System.Convert.ToBase64String(plainTextBytes);
	}

	public static string Base64Decode(string base64EncodedData) {
		var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
		return System.Text.Encoding.ASCII.GetString(base64EncodedBytes);
	}

	public static string Encrypt(this string text)
	{
		SymmetricAlgorithm algorithm = DES.Create();
		ICryptoTransform transform = algorithm.CreateEncryptor(key, iv);
		byte[] inputbuffer = Encoding.Unicode.GetBytes(text);
		byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
		return Convert.ToBase64String(outputBuffer);
	}

	public static string Decrypt(this string text)
	{
		SymmetricAlgorithm algorithm = DES.Create();
		ICryptoTransform transform = algorithm.CreateDecryptor(key, iv);
		byte[] inputbuffer = Convert.FromBase64String(text);
		byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
		return Encoding.Unicode.GetString(outputBuffer);
	}

	public static string EncodeBools(bool[] bools) {
		string bin = "";
		for(int i = 0; i < bools.Length; i++)
			bin += bools[i] ? '1' : '0';
		BigInteger big = new BigInteger(bin, 2);
		// return Base64Encode(big.ToString(36));
		return big.ToString(36);
	}

	public static bool[] DecodeBools(string boolHash, int size) {
		// string base36 = Base64Decode(boolHash);
		BigInteger big = new BigInteger(boolHash, 36);
		string bin = big.ToString(2);
		while(bin.Length < size) // need to know size, because 
			bin = "0" + bin;
		bool[] bools = new bool[bin.Length];
		for(int i = 0; i < bin.Length; i++) 
			bools[i] = bin[i] == '1' ? true : false;
		return bools;
	}

	
}